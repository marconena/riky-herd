CXX		  	:= g++
CXX_FLAGS 	:= -Wall -Wextra -std=c++17 -ggdb

BIN			:= bin
SRC			:= src
INCLUDE		:= include:src
LIB			:= lib
OBJ_DIR     := .obj

LIBRARIES	:=
EXECUTABLE	:= main

OBJS		:= $(OBJ_DIR)/log.o $(OBJ_DIR)/FileTree.o

BTREE_FILES	:= $(SRC)/b+tree/*

TESTS		:= true

ifeq ($(OS),Windows_NT)
    # Windows stuff
    EXE_EXT := .exe
else
    # Linux stuff
	EXE_EXT :=
    
endif


ifeq ($(TESTS),true)

	OBJS := $(OBJS) $(OBJ_DIR)/main.o $(OBJ_DIR)/testTree.o
endif


all: $(BIN)/$(EXECUTABLE)$(EXE_EXT)
	# $(OS): FINISH----------------------------------------------

run: clean all
	clear
	./$(BIN)/$(EXECUTABLE)

$(BIN)/$(EXECUTABLE)$(EXE_EXT): $(OBJS)
	$(CXX) $(CXX_FLAGS) -I$(INCLUDE) -L$(LIB) $^ -o $@ $(LIBRARIES)




$(OBJ_DIR)/log.o: $(SRC)/utils/log.cpp $(SRC)/utils/log.h
	$(CXX) -c $(CXX_FLAGS) -I$(INCLUDE) -L$(LIB) $< -o $@


$(OBJ_DIR)/FileTree.o: $(SRC)/impl/FileTree.cpp $(SRC)/impl/FileTree.h $(BTREE_FILES)
	$(CXX) -c $(CXX_FLAGS) -I$(INCLUDE) -L$(LIB) $< -o $@


#
#  TESTS
#
# Test suite
$(OBJ_DIR)/main.o: $(SRC)/main.cpp
	$(CXX) -c $(CXX_FLAGS) -I$(INCLUDE) -L$(LIB) $< -o $@

$(OBJ_DIR)/testTree.o: $(SRC)/tests/testTree.cpp
	$(CXX) -c $(CXX_FLAGS) -I$(INCLUDE) -L$(LIB) $< -o $@

clean:
	-rm $(BIN)/*
	-rm $(OBJ_DIR)/*
