#include <stdio.h>
#include <string.h>

void toMaxUnsign(void *p, int size) {
	memset(p,255,size);
}

void toMinMax(void *min, void * max, int size) {
	memset(max,255,size);
	((unsigned char*)max)[size-1]=127;
	memset(min,0,size);
	((unsigned char*)min)[size-1]=128;
}

void testTypesSize() {
	printf("C++Types-------------------------------------------------------------\n");
	{
		bool size=sizeof(char);
		char s0,s1;
		toMinMax(&s0, &s1, size);
		printf("char: %i bytes (%i,%i)\n",size,s0,s1);
		char unsigned s;
		toMaxUnsign(&s,size);
		printf("char unsigned: %i bites (0,%u)\n",size,s);
		
	}
	{
		int size=sizeof(char);
		char s0,s1;
		toMinMax(&s0, &s1, size);
		printf("char: %i bytes (%i,%i)\n",size,s0,s1);
		char unsigned s;
		toMaxUnsign(&s,size);
		printf("char unsigned: %i bites (0,%u)\n",size,s);
		
	}
	int size=sizeof(short);
	short s0,s1;
	toMinMax(&s0, &s1, size);
	printf("short: %i bytes (%i,%i)\n",size,s0,s1);
	short unsigned s;
	toMaxUnsign(&s,size);
	printf("short unsigned: %i bites (0,%u)\n",size,s);
	
	size=sizeof(int);
	int i0,i1;
	toMinMax(&i0, &i1, size);
	printf("int: %i bytes (%i,%i)\n",size,i0,i1);
	int unsigned i;
	toMaxUnsign(&i,size);
	printf("int unsigned: %i bytes (0,%u)\n",size,i);
	
	size=sizeof(long long);
	long long l0,l1;
	toMinMax(&l0, &l1, size);
	printf("long long: %i bytes ( %lli,%lli \n",size,l0,l1);
	long long unsigned l;
	toMaxUnsign(&l,size);
	printf("long long unsigned: %i bytes  ( 0,%llu )\n",size,l);
	
}
