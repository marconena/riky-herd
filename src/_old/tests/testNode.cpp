#include "tests.h"
#include "log.h"
#include "util.h"

#ifdef WINDOWS
#include <windows.h>
#include <psapi.h>
#endif

#include <sstream>


void dumpNode(BPlusNode<LKey,StrValue> *n, int level) {
	if(level ==0) {
		log.info("DUMP----------------------------------------------------");
	}
	BPlusNode<LKey,StrValue> *node=n;
	std::ostringstream buf;
	for(int i=0;i<level;i++) {
		buf<<" ";
	}
	
	buf<<node->dump();
	
	log.info(buf.str().c_str());
	
	if(!n->isLeaf() && n->getLen()>0) {
		for(uchar i=0;i<n->getLen();i++) {
			dumpNode(n->node(i), level+1);
		}
	}
}

typedef BPlusNode<LKey,StrValue> TestNode;



TestNode *add(TestNode *node,long val, const char *str , bool dump) {

	log.info(string_format("+[%ld,%s]",val, str).c_str());
	node->add(val,str);
	node= node->getGranParent();
	if(dump) {
		dumpNode(node,0);
	}
	return node;
}

TestNode *add(TestNode *node,long val, const char *str ) {
	return add(node,val,str,false);
}

TestNode *del(TestNode *node,long val, bool dump) {
	
	log.info(string_format("-[%ld]",val));
	
	if(!node->del(val)) {
		log.info(string_format("  %ld NOT FOUND !",val));
	}
	if(!node->isLeaf() && node->getLen()==1) {
		TestNode *me=node;
		node=me->node(0);
		me->clear();
		delete me;
		
	}
	if(dump) {
		dumpNode(node,0);
	}
	return node;
}
TestNode *del(TestNode *node,long val ) {
	return del(node,val,false);
}

void PrintMemoryInfo(  )
{
	#ifdef WINDOWS
    HANDLE hProcess;
    PROCESS_MEMORY_COUNTERS pmc;
	DWORD processID=GetCurrentProcessId();

    // Print the process identifier.

    log.info( "Process ID: %u", processID );

    // Print information about the memory usage of the process.

    hProcess = OpenProcess(  PROCESS_QUERY_INFORMATION |
                                    PROCESS_VM_READ,
                                    FALSE, processID );
    if (NULL == hProcess)
        return;

    if ( GetProcessMemoryInfo( hProcess, &pmc, sizeof(pmc)) )
    {
        log.info( "\tPageFaultCount: 0x%08X", pmc.PageFaultCount );
        log.info( "\tPeakWorkingSetSize: 0x%08X", 
                  pmc.PeakWorkingSetSize );
        log.info( "\tWorkingSetSize: 0x%08X", pmc.WorkingSetSize );
        log.info( "\tQuotaPeakPagedPoolUsage: 0x%08X", 
                  pmc.QuotaPeakPagedPoolUsage );
        log.info( "\tQuotaPagedPoolUsage: 0x%08X", 
                  pmc.QuotaPagedPoolUsage );
        log.info( "\tQuotaPeakNonPagedPoolUsage: 0x%08X", 
                  pmc.QuotaPeakNonPagedPoolUsage );
        log.info( "\tQuotaNonPagedPoolUsage: 0x%08X", 
                  pmc.QuotaNonPagedPoolUsage );
        log.info( "\tPagefileUsage: 0x%08X", pmc.PagefileUsage ); 
        log.info( "\tPeakPagefileUsage: 0x%08X", 
                  pmc.PeakPagefileUsage );
    }

    CloseHandle( hProcess );
	#endif
}

class LocalTree: public AbstractTree {
public:
	uchar getOrder() {
		return 3;
	}
	
	bool isStorable() {
		return false;
	}
	
};


void testNode() {
	
	PrintMemoryInfo();
	
	LocalTree tree;
	
	TestNode *root(new TestNode(true,&tree));
/*
	long size=50;
	for(long i=0;i<size;i++)
		root=add(root,i,string_format("Desc%li",i).c_str());
	//dumpNode(root,0);
	
	for(long i=0;i<size;i++)
		root=del(root,i);

	 */
	log.info("testNode ADD-------------------------------------------------------------");
	
	root=add(root,40,"Ubaldo Pescatore");
	
	
	root=add(root,10,"Antonio Piovesan");
	
	root=add(root,30,"Massimo Dell'andrea");
	
	root=add(root,20,"Mario Franceschin");
	
	root=add(root,35,"Nicola Centenaro");
	
	root=add(root,20,"Marco Franceschin",true);
	
	root=add(root,1,"1",true);
	
	root=add(root,41,"41",true);
	root=add(root,41,"41 BIS",true);
	root=add(root,42,"42");
	root=add(root,43,"43");
	root=add(root,44,"44");
	root=add(root,45,"45");
	root=add(root,46,"46");
	root=add(root,47,"47");
	root=add(root,48,"48");
	root=add(root,-10,"-10");
	root=add(root,-11,"-11");
	root=add(root,-12,"-12");
	root=add(root,-13,"-13");
	root=add(root,-14,"-14");
	root=add(root,-3,"");
	root=add(root,19,"Prima di marco Franceschin");
	root=add(root,42,"42 (UPDATED)",false);
	log.info("testNode DELETE-------------------------------------------------------------");
	/*
	 * */
	dumpNode(root,0);	
	root=del(root,44);
	root=del(root,48);
	root=del(root,44);
	dumpNode(root,0);
	root=del(root,-10);
	
/* 
 */
	log.info("testNode FREE NODE ROOT-------------------------------------------------------------");
	
	PrintMemoryInfo();
	delete root;
	PrintMemoryInfo();
	
}