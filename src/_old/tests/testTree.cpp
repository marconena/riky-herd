#include "tests.h"
#include "../utils/log.h"

#include "../impl/FileTree.h"

void testBTree()
{
    logInfo("START: testBTree %i");
    FileTree tree("db.rh", true);
    std::ifstream file("J4SHOPRUNTIME.ITEMS");
    long row=0;
    if (file.is_open())
    {
        std::string buf;
        while (!file.eof())
        {
            int i = file.get();
            if (i != 10)
            {
                buf += (char)i;
            }
            else
            {
                if (buf.size() > 0)
                {
                    if(row % 1000 ==0) {
                        logInfo("row %li: %s:%li", row, buf.c_str(), file.tellg());
                    }
                    row++;
                }
                buf.clear();
            }
        }

        file.close();
    }
    logInfo("END: testBTree %li row/s",row);
}
