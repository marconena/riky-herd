#include "tests.h"

#include <stdio.h>
#include <string>



void testAbstractVal() {
	printf("AbstractKey-------------------------------------------------------------\n");
	StrValue a("Marco"),b("Franceschin");
	
	printf("a=%s,b=%s, eq=%s\n",a.getStr(), b.getStr(), a.cmp(b)==0 ? "true":"false");
	a.set("Franceschin");
	printf("a=%s,b=%s, eq=%s\n",a.getStr(), b.getStr(), a.cmp(b)==0 ? "true":"false");
	
}

void testBTreeValue() {
	printf("BTreeValue-------------------------------------------------------------\n");
	BTreeValue<StrValue> v1,v2;
	
	printf("a=%s,b=%s, eq=%s\n",v1.get().getStr(), v2.get().getStr(), v1.cmp(v2)==0 ? "true":"false");
	v1.set("Marco");
	v2.set("Franceschin");
	printf("a=%s,b=%s, eq=%s\n",v1.get().getStr(), v2.get().getStr(), v1.cmp(v2)==0 ? "true":"false");
	v1.set(v2);
	printf("a=%s,b=%s, eq=%s\n",v1.get().getStr(), v2.get().getStr(), v1.cmp(v2)==0 ? "true":"false");
}

void testValues() {
	testAbstractVal();
	testBTreeValue();
	
}