#ifndef _TESTS_H_
#define _TESTS_H_

#include "../b+tree/b+tree.h"
#include <string>


void testTypesSize() ;

//void testKey();

void testValues();

void testBTree();

void testNode();

class StrValue: public AbstractValue<StrValue> {
protected:
	std::string str;
public:
	StrValue() {
		
	}
	StrValue(const char *v) {
		str=v;
	}
	
	char cmp(const StrValue &val) const {
		return str.compare(val.str);
	}
	void set(const StrValue &val)  {
		str=val.str;
	}
	const StrValue & get()  const {
		return *this;
	}
	void set(const char *v) {
		str=v;
	}
	
	const char *getStr() const {
		return str.c_str();
	}
};


class LKey: public AbstractValue<LKey> {
protected:
	long  key;
public:
	LKey() {
		key=-1;
	}
	
	LKey(long val) {
		key=val;
	}
	
	char cmp(const LKey &val) const  {
		if(key>val.key)
			return 1;
		if(key<val.key)
			return -1;
		return 0;
	}
	
	void set(const LKey &val) {
		key=val.key;
	}
	const LKey & get() const {
		return *this;
	}
	
	long value() const {
		return key;
	}
	
	void value(long val) {
		key=val;
	}
		
};



#endif