/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FileTree.cpp
 * Author: mfranceschin
 * 
 * Created on December 17, 2018, 3:35 PM
 */

#include "FileTree.h"

#include <string.h>


/**
 * Open a file
 * @param file
 * @param override
 * @return 
 */
bool FileTree::open(std::string file, bool override) {
    std::fstream fstream;
    fstream.open(file.c_str(),std::fstream::in | std::fstream::binary);
    if(fstream.is_open() && !override) {
            iid=0;
            fstream.close();
    } else {
            iid=0;
            std::ofstream ostream;
            // FIle not exist create it
            fstream.open(file.c_str(),std::fstream::binary | std::fstream::out | std::fstream::trunc );
            if(fstream.is_open()) {
                long pos=strlen(_PREFIX_CONTEXT_);
                fstream.write(_PREFIX_CONTEXT_,pos);
                FileStream fs(fstream);
                iid.push(fs);
                pStored.push(fs);
                pFree.push(fs);
                heapStart=pos+iid.size()+pStored.size()+pFree.size();
                

                fstream.close();
                return true;
            }
    }
    return false;
}



