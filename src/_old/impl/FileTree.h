/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FileTree.h
 * Author: mfranceschin
 *
 * Created on December 17, 2018, 3:35 PM
 */

#ifndef FILETREE_H
#define FILETREE_H

#define _PREFIX_CONTEXT_ "RH000001"

#include "../b+tree/b+tree.h"

#include <fstream>

class LongValue : public AbstractValue<LongValue>, public AbstractStorable
{
  protected:
    long value;

  public:
    LongValue(long lval)
    {
        value = lval;
    }
    LongValue()
    {
        value = 0;
    }

    char cmp(const LongValue &val) const
    {
        if (value < val.value)
            return -1;
        if (value > val.value)
            return 1;
        return 0;
    }
    void set(const LongValue &val)
    {
        value = val.value;
    }

    void set(long val)
    {
        value = val;
    }
    const LongValue &get() const
    {
        return *this;
    }

    long size() const
    {
        return sizeof(long);
    }

    bool push(AbstractStream &out)
    {
        unsigned char *p = (unsigned char *)&value;
        for (uchar i = 0; i < (uchar)sizeof(long); i++)
        {
            out.write(p[(int)i]);
        }
        return true;
    }

    bool pull(AbstractStream &in)
    {
        unsigned char *p = (unsigned char *)&value;
        for (uchar i = 0; i < (uchar)sizeof(long); i++)
        {
            p[i] = in.read();
        }
        return true;
    }
};

class BPlusFileTree : public BPlusTree<LongValue, LongValue>
{

  public:
    BPlusFileTree(unsigned char order, bool duplicatekey) : BPlusTree<LongValue, LongValue>(order, duplicatekey)
    {
    }
};

class FileStream : public AbstractStream
{
  private:
    std::fstream &stream;

  public:
    FileStream(std::fstream &stream) : stream(stream)
    {
    }

    bool write(unsigned char c)
    {
        if (stream.is_open())
        {
            stream.write((const char *)&c, 1);
            return true;
        }
        return false;
    }

    unsigned char read()
    {
        if (stream.is_open() && !stream.eof())
        {
            return (unsigned char)stream.get();
        }
        return (unsigned char)0;
    }

    std::fstream &getStream() {
        return stream;
    }

    bool seekP(long pos) {
        stream.seekp(pos);
        return true;
    }

    bool seekG(long pos) {
        stream.seekg(pos);
        return true;
    }

    long tellG() {
        return stream.tellg();
    }
    
    long tellP() {
        return stream.tellp();
    }


};


/**
 * File storage class (use b+ tree for save a values)
 */
class FileTree
{
  protected:
    BPlusFileTree storedTree;
    BPlusFileTree freeTree;
    LongValue iid;
    LongValue pStored;
    LongValue pFree;
    LongValue heapStart;

  public:
    
    /**
     * 
     */
    FileTree(std::string file, bool create) : storedTree(10, false), freeTree(10, true)
    {
        open(file, create);
    };
    FileTree(std::string file) : storedTree(10, false), freeTree(10, true)
    {
        open(file, false);
    }
    
    

  private:
    bool open(std::string file, bool create);

    
};

#endif /* FILETREE_H */
