#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <stdarg.h>
#include "log.h" 
#include <string>
#include <vector>

void log(LogLevel level, const char *setting, va_list va);

std::string m_path("output.log");

std::vector<LogOutput> m_outputs;

LogLevel m_level(LogLevel::LEVEL_INFO);

int logAddOutput(fnWriteLog fn, void * data)
{
	LogOutput out;
	out.data=data;
	out.fn=fn;
	m_outputs.push_back(out);
	return m_outputs.size();
}

bool logRemoveOutput(fnWriteLog fn, void * data)
{
	for(std::vector<LogOutput>::iterator i=m_outputs.begin();i!=m_outputs.end();i++) {
		if((*i).fn==fn && (data == NULL || data==(*i).data) ) {
			i=m_outputs.erase(i);
			return true;
		}
	}
	return false;
}

void setLogFile(const char *path)
{
	m_path=path;
}
void setLogLevel(LogLevel level)
{
	m_level=level;
}

void clearLog()
{
	remove(m_path.c_str());
}


void logTrace(const char *setting, ...)
{
	va_list va;
	va_start(va,setting);
	log(LogLevel::LEVEL_TRACE,setting,va);
	va_end(va);
}

void logDebug(const char *setting, ...)
{
	va_list va;
	va_start(va,setting);
	log(LogLevel::LEVEL_DEBUG,setting,va);
	va_end(va);
}

void logInfo(const char *setting, ...)
{
	va_list va;
	va_start(va,setting);
	log(LogLevel::LEVEL_INFO,setting,va);
	va_end(va);
}
void logWarning(const char *setting, ...)
{
	va_list va;
	va_start(va,setting);
	log(LogLevel::LEVEL_WARNING,setting,va);
	va_end(va);
}
void logError(const char *setting, ...)
{
	va_list va;
	va_start(va,setting);
	log(LogLevel::LEVEL_ERROR,setting,va);
	va_end(va);
}

int getMilliCount(){
	timeb tb;
	ftime(&tb);
	int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
	return nCount;
}


void log(LogLevel level, const char *setting, va_list va)
{
	if(level<m_level || m_level==LogLevel::LEVEL_NONE)
		return;
	FILE *f;


	char* buffer;
	int bufsize;
	bufsize = 1100; /* fixed size */
	bufsize = sizeof(va) + sizeof(setting) + 1100;
	buffer = (char*)malloc(bufsize);
	time_t now;
	struct tm *day;
	struct timeval tms;

	gettimeofday(&tms, NULL);

	now=tms.tv_sec;
	//time(&now);
	day=localtime(&now);

	//GetSystemTime(&systemTime);
	sprintf(buffer, "[%04d/%02d/%02dT%02d:%02d:%02d.%03d] ",
	        day->tm_year+1900, day->tm_mon+1, day->tm_mday,
	        day->tm_hour, day->tm_min, day->tm_sec,(int)(tms.tv_usec / 1000));
	switch(level) { 
		case LogLevel::LEVEL_TRACE:
			strcat(buffer,"TRACE: ");
			//printf("TRACE ");
			break;
		case LogLevel::LEVEL_DEBUG:
			strcat(buffer,"DEBUG: ");
			//printf("TRACE ");
			break;
		case LogLevel::LEVEL_INFO:
			strcat(buffer,"INFO: ");
			//printf("INFO ");
			break;
		case LogLevel::LEVEL_WARNING:
			strcat(buffer,"WARNING: ");
			//printf("WARNING ");
			break;
		case LogLevel::LEVEL_ERROR:
			strcat(buffer,"ERROR: ");
			//printf("ERROR ");
			break;
		default:
			break;
		}
	//vsprintf(buffer+strlen(buffer), setting, va);
	int preLen=strlen(buffer);
	int size=vsnprintf(buffer+preLen,bufsize-preLen,setting, va);
	if(size>bufsize) {
		bufsize=size+preLen+2;
		buffer=(char*)realloc(buffer,bufsize);
		size=vsnprintf(buffer+preLen,bufsize-preLen,setting, va);
	};
	(buffer+preLen+size)[0]='\n';
	(buffer+preLen+size)[1]='\0';
	//strcat(buffer,"\n");
	if((f=fopen(m_path.c_str(),"a"))) {
		fputs(buffer,f);
		fclose(f);
	}
	// Write to all outputs
	for(std::vector<LogOutput>::iterator i=m_outputs.begin();i!=m_outputs.end();i++) {
		(*i).fn(level,buffer,(*i).data);
	}
	free(buffer);
}



void freeLog()
{
	FILE *f;

	if((f=fopen(m_path.c_str(),"w"))) {
		fclose(f);
	}
}
