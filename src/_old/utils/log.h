#ifndef _LOG_H_
#define _LOG_H_




enum class LogLevel: short {
    LEVEL_NONE =0,
    LEVEL_TRACE ,
    LEVEL_DEBUG ,
    LEVEL_INFO,
    LEVEL_WARNING,
    LEVEL_ERROR
} ;

void setLogFile(const char *path);
void setLogLevel(LogLevel level);
void clearLog();


void logTrace(const char *setting, ...);
void logDebug(const char *setting, ...);
void logInfo(const char *setting, ...);
void logWarning(const char *setting, ...);
void logError(const char *setting, ...);
void freeLog();


typedef bool (*fnWriteLog)(LogLevel level, const char* logValue, void *userData);
struct LogOutput {
	fnWriteLog fn;
	void *data;
};

int logAddOutput(fnWriteLog fn, void * data=NULL);
bool logRemoveOutput(fnWriteLog fn, void * data=NULL);



#endif
