#ifndef _UTIL_H_
#define _UTIL_H_

#include <string>

std::string string_format(const std::string fmt, ...) ;
#endif
