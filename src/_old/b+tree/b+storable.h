#ifndef _BPLUSSTORABLE_H_
#define _BPLUSSTORABLE_H_

#include <iostream>

class AbstractStream {
	
public:
	virtual bool write(unsigned char c)=0;
	virtual unsigned char read() =0;
};

class AbstractStorable {
	
	public:
	virtual long size() const =0;
	virtual bool push(AbstractStream &out) =0;
	virtual bool pull(AbstractStream &in) =0;
};

#endif