#ifndef _BPLUSTREE_H_
#define _BPLUSTREE_H_
#include "b+storable.h"
#include "b+value.h"
#include "b+node.h"
#include "../utils/log.h"
#include <iostream>


template<class K,class V>
class BPlusTree {
private:
	uchar order;
	BPlusNode<K,V> *root;
	bool duplicate;
public:
	BPlusTree(uchar order, bool duplicatekey) {
		this->duplicate=duplicatekey;
		this->order=order;
		this->root=new BPlusNode<K,V>(true, order, duplicate);
	}
	~BPlusTree() {
		delete root;
	}
	
	uchar getOrder() {
		return order;
	}
	BPlusNode<K,V> &getRoot() {
		return *root;
	}
	
};

#endif