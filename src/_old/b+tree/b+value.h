#ifndef _BPVALUE_H_
#define _BPVALUE_H_



template<class VClass>
class AbstractValue {
public:
	AbstractValue() {
		
	}
	virtual char cmp(const VClass &val) const =0;
	virtual void set(const VClass &val) =0;
	virtual const VClass & get() const =0;
};

template<class V>
class BTreeValue {
protected:
	V val;
public:
	BTreeValue() {
		
	}
	
	BTreeValue(const V &v) {
		val.set(v);
	}
	
	const V &get() const{
		return val;
	}
	
	void set(const BTreeValue &v) {
		val.set(v.get());
	}
	
	void set(const V &v) {
		val.set(v);
	}
	
	char cmp(const BTreeValue &val) const {
		return val.cmp(val.val);
	}
	
};



#endif