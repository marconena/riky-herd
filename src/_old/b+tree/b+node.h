#ifndef _BPLUSNODE_H_
#define _BPLUSNODE_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../utils/log.h"

#define uchar unsigned char

#include <iostream>


template<class K,class V>
class BPlusNode {
private:
	bool leaf;
	uchar order;
	bool duplicate;
protected:
	uchar len;
	K *keys;
	V *vals;
	BPlusNode<K,V>  **nodes;
	BPlusNode<K,V> *parent, *prev, *next ;
public:
	BPlusNode(bool isLeaf, uchar aorder, bool aduplicate) {
		duplicate=aduplicate;
		order=aorder;
		parent=prev=next=NULL;
		len=0;
		leaf=isLeaf;
		if(leaf) {
			keys=new K[order];
			nodes = NULL;
			vals=new V[order];
		} else {
			keys=new K[order];
			nodes = new BPlusNode<K,V>*[order+1];
			vals=NULL;
		}
	}
	
	~BPlusNode()  {
		
		if(prev!=NULL)
			prev->next=next;
		if(next!=NULL)
			next->prev=prev;
		delete[] keys;
		if(isLeaf()) {
			delete[] vals;
			vals=NULL;
		} else {
			for(uchar i=0;i<len;i++) {
				
				delete nodes[i];
				nodes[i]=NULL; 
			}
			
			delete[] nodes;
			//free(nodes);
			nodes=NULL;
		}
		len=0;
		parent=next=prev=NULL;
	}
	
	void clear() {
		parent=NULL;
		if(!leaf) {
			for(char i=0;i<len;i++) {
				node(i)->parent=NULL;
			}
		}
		
		len=0;
	}
	
	bool isLeaf() {
		return leaf;
	}
	
	bool isFull() {
		return len==order;
	}
	bool isEmpty() {
		return len==0;
	}
	
	uchar find(BPlusNode<K,V> *n, bool &finded) {
		if(!isLeaf()) {
			for(uchar i=0;i<len;i++) {
				if(nodes[i]==n) {
					finded=true;
					return i;
				}
			}
		}
		finded=false;
		return len;
	}
	
	BPlusNode<K,V> *getNodes() {
		return (BPlusNode<K,V> **)nodes;
	}
	
	
	uchar find(const K &akey, bool &eq) {
		for(uchar idx=0;idx<(leaf ? len : len-1); idx++) {
			char cmp=akey.cmp(keys[idx]);
			if(cmp<=0) {
				eq=(cmp==0);
				return idx ;
			}
		}
		eq=false;
		
		return len;
	}
	
	
	
	bool add(BPlusNode<K,V> *anode) {
		if(!isLeaf()) {			
			if(!isFull() ) {
				if(len==0) {
					nodes[0]=anode;
				} else {
					bool eq;
					uchar idx=find(anode->key(0),eq) ;
					for(uchar i=len;i>idx;i--) {
						keys[i].set(keys[i-1]);
						nodes[i+1]=nodes[i];
					}
					if(idx==0 ) {
						if(anode->key(0).cmp(node(0)->key(0)) < 0) {
							keys[0].set(node(0)->key(0));
							
							nodes[1]=nodes[0];
							nodes[0]=anode;
							if(parent!=NULL) {
								parent->updateKey(this,anode->key(0));
							}
						} else {
							keys[0].set(anode->key(0));
							nodes[1]=anode;
						}
					} else {
						keys[idx-1].set(anode->key(0));
						nodes[idx]=anode;
					}
				}
				len++;
				split();
				return true;
			} else {
				logDebug("CANNOT ADD NODE ON NODE FULL !!!!");
			}
		} else {
			logDebug("CANNOT ADD A NODE ON LEAF !!!!");
		}
		return false;
	}
	
	BPlusNode<K,V> *findLeaf(const K & akey) {
		if(isLeaf())
			return this;
		bool eq;
		uchar idx=find(akey,eq);
		if(eq) idx++;
		
		if(idx<len) {
			return node(idx)->findLeaf(akey);
		} else {
			return node(len-1)->findLeaf(akey);
		}
		return NULL;
	}
	
	void updateKey(BPlusNode<K,V> *node,const K & key) {
		if(!isLeaf()) {
			for(uchar i=0;i<len;i++) {
				if(nodes[i]==node) {
					if(i==0 ) {
						if(parent!=NULL) {
							parent->updateKey(this,key);
						}
					} else {
						keys[i-1].set(key);
					}
					return;
				}
			}
		}
	}
	
	bool add(const K & key, const V &val) {
		if(!isLeaf()) {
			BPlusNode<K,V> *node=findLeaf(key);
			if(node!=NULL) {
				return node->add(key,val);
			}
			
		} else {
			if(!isFull()) {
				bool eq;
				uchar idx=find(key,eq);
				if(eq) {
					vals[idx].set(val);
				} else {
					for(uchar i=len;i>idx;i--) {
						keys[i].set(keys[i-1]);
						vals[i].set(vals[i-1]);
					}
					keys[idx].set(key);
					vals[idx].set(val);
					len++;
					split();
				}
				if(!eq && idx==0 && parent!=NULL) {
					parent->updateKey(this,key);
				}
				return true;
			} else {
				logDebug("IS FULL !!!!");
			}
		}
		return false;
	}
	
	/**
	 * @brief Split a node to 2 nodes
	 */
	bool split() {
		// Splitt only a full node
		if(isFull()) {
			
			if(parent==NULL) {
				parent=new BPlusNode<K,V>(false,order);
				parent->add(this);
			}
			BPlusNode<K,V> *lnode=new BPlusNode<K,V>(leaf, order);
			
			uchar midle=len/2;
			for(uchar i=midle;i<len;i++) {
				uchar pos=i-midle;
				if(leaf) {
					lnode->keys[pos].set(keys[i]);
					lnode->vals[pos].set(vals[i]);
					keys[i].set(K());
				} else {
					if(pos>0) {
						lnode->keys[pos-1].set(keys[i-1]);
					}
					lnode->nodes[pos]=nodes[i];
					lnode->nodes[pos]->parent=lnode;
				}
				
			}
			lnode->len=len-midle;
			len=midle;
			lnode->parent=parent;
			lnode->prev=this;
			lnode->next=next;
			if(next)
				next->prev=lnode;
			next=lnode;
			parent->add(lnode);
			
			return true;
		}
		return false;
		
	}
	bool joinRight() {
		if(next!=NULL) {
			if((len+next->len)<order) {
				for(uchar i=0;i<next->len;i++) {
					if(leaf) {
						keys[len].set(next->keys[i]);
						vals[len].set(next->vals[i]);
					} else {
						if(len>0) {
							keys[len-1].set(next->key(i));
						}
						nodes[len]=next->nodes[i];
						nodes[len]->parent=this;
					}
					len++;
				}
				
				next->len=0;
				if(next->parent!=NULL) {
					next->parent->del(next);
				}
				return true;
			}
		}
		return false;
	}
	bool join() {
		
		if(prev!=NULL) {
			if(prev->joinRight()) {
				return true;
			}
		}
		if(joinRight()) {
			return true;
		}
		
		return false;
	}
	
	
	bool del(BPlusNode<K,V> *n) {
		if(!isLeaf()) {
			bool finded=false;
			uchar idx=find(n,finded);
			if(finded) {
				
				len --;
				
				for(uchar i=idx;i<len;i++) {
					if(i>0  && i<(len-1)) {
						keys[i-1].set(keys[i]);
					}
					nodes[i]=nodes[i+1];
				}
				
				delete n;
				if(parent!=NULL) {
					if(isEmpty() ) {
						parent->del(this);
					} else if(!join()) {
						if(idx==0) {
							parent->updateKey(this,keys[0]);
						}
					} 
				}
				
				return true;
			}
		}
		return false;
	}
	
	bool del(const K & key) {
		if(!isLeaf()) {
			BPlusNode<K,V> *node=findLeaf(key);
			if(node!=NULL) {
				return node->del(key);
			}
			
		} else {
			
			bool eq;
			uchar idx=find(key,eq);
			if(eq) {
				// Delete item 
				len --;
				for(uchar i=idx;i<len;i++) {
					keys[i].set(keys[i+1]);
					vals[i].set(vals[i+1]);
				}
				
				if(parent!=NULL) {
					if(isEmpty() ) {
						parent->del(this);
					} else if(!join()) {
						if(idx==0 ) {
							parent->updateKey(this,keys[0]);
						}
					}
				}
				
				return true;
			} 
		}
		return false;
	}
	
	uchar getLen() {
		return len;
	}
	
	const K key(uchar pos) {
		if(leaf) {
			if(pos<len) {
				return keys[pos];
			}
		} else {
			if(pos<len) {
				if(pos==0) {
					return node(0)->key(0);
				}
				return keys[pos-1];
			}
		}
		return K();
	}
	
	const V val(uchar pos) {
		if(leaf && pos<len) {
			return vals[pos];
		}
		return V();
	}
	
	BPlusNode<K,V> *node(uchar pos) {
		if(pos<len && !leaf) {
			return nodes[pos];
		}
		return NULL;
	}
	
	
	
	BPlusNode<K,V> *getNext() {
		return next;
	}
	
	BPlusNode<K,V> *getPrev() {
		return prev;
	}
	
	BPlusNode<K,V> *getParent() {
		return parent;
	}
	
	BPlusNode<K,V> *getGranParent() {
		BPlusNode<K,V> * ret=this;
		while(ret->parent!=NULL) {
			ret=ret->parent;
		}
		
		return ret;
	}

	
};




#endif