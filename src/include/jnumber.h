#ifndef _JNUMBER_H_
#define _JNUMBER_H_
#include "memory-alloc.h"

typedef unsigned long long NumberValue;

class Number : public ByteBuf
{

public:
    Number(NumberValue id);
    static NumberValue from(const ByteBuf *buf);

    static char sizeOf(NumberValue id);
    static int cmp(const ByteBuf &a, const ByteBuf &b);

};


#endif