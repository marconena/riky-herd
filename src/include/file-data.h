#ifndef _FILE_DATA_H_
#define _FILE_DATA_H_

#include "file-storage-tree.h"
#include <string>

class StringKey: public SerializableKey {
public:
    static int Cmp(const BTVal &a, const BTVal &b);
    StringKey();
    StringKey(void *val,MemSize size);
    StringKey(const char *val);
    const std::string value();
};

class StringVal: public SerializableVal {
public:
    StringVal();
    StringVal(const char *val);
    StringVal(void *val,MemSize size);
    const std::string value();

};


class FileDataNode;

class FileData: public FileStorageTree {
public:
    FileData(StorageManager *manager);

    bool put(const StringKey &key, const StringVal &val);
    bool del(const StringKey &key);

    virtual FileStorageNode *createNode( bool leaf, const PageLink &link);


protected:
    
    

    

};

class FileDataNode: public FileStorageNode {
    public:
        FileDataNode(FileData *parent, bool leaf);
        FileDataNode(FileData *parent,const PageLink &link);

        virtual SerializableKey *newKey();
        virtual SerializableVal *newVal();
    
};



#endif