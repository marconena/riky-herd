#ifndef _DB_FILE_H_
#define _DB_FILE_H_
#include <string>
#include <fstream>
#include "pages-cache.h"
#include "file-index.h"
#include "file-free-index.h"
#include "file-data.h"



class FileHeader: public ByteBuf {
public:
    FileHeader();
    PageLink &indexLink();
    void indexLink(const PageLink &link);
    PageLink &freeLink();
    void freeLink(const PageLink &link);
    PageLink &dataLink();
    void dataLink(const PageLink &link);
    PageId pageCount();

    PageId newId();
    bool dirty() {
        return _dirty;
    }
    void dirty(bool val) {
        _dirty=val;
    }

private:
    PageLink _index;
    PageLink _free;
    PageLink _data;
    PageId *_pageCount;
    bool _dirty;


};

/**
 * @brief Database file
 * 
 */
class DBFile: public StorageManager, public PageRetriever {
public:
    DBFile(const std::string path);
    ~DBFile();
    bool openOrCreate();

    bool push(const std::string &key, const std::string &value);
    bool del(const std::string &key);


    void onChanged(StorageBTNode *node, NodeStoreOperation oper);
    bool retrieve(StorageBTNode *node);

    bool commit();

    bool get(const StringKey &key, StringVal &val );

    PageCache *getPage(PageId id);

    void close();

    const std::string read(FileStorageTree &tree, const PageLink &link);
    bool write(FileStorageTree &tree);

    FileData &data() {
        return _data;
    }

private:
    /**
     * Private declaration
     */
    std::string _path;
    PagesCache _cache;
    FileHeader _header;
    FileIndex _index;
    FileFreeIndex _free;
    FileData _data;
    std::fstream _file;
    bool push(const ByteBuf &buf, PageLink &link);
    std::string _error;
};

#endif