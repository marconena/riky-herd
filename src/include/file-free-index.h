#ifndef _FILE_FREE_INDEX_H_
#define _FILE_FREE_INDEX_H_
#include "btree.h"
#include "jnumber.h"
#include "pages-cache.h"

class PageSizeKey: public BTKey {
public:
    static int Cmp(const BTVal &a, const BTVal &b) {
        MemSize as(*(MemSize*)a.buf());
        MemSize bs(*(MemSize*)b.buf());
        if(as>bs) return 1;
        if(as<bs) return -1;
        return 0;
    }

    PageSizeKey(MemSize size): BTKey(sizeof(MemSize),PageSizeKey::Cmp) {
        _freeSize=(MemSize *)buf();
        *_freeSize=size;
    }

    MemSize freeSize() {
        return *_freeSize;
    }

private: 
    MemSize *_freeSize;
};


class PageIdValue:  public BTVal {
public:
    PageIdValue(PageId id): BTVal(sizeof(PageId)) {
        _id=(PageId*)buf();
        *_id=id;
    }

    PageId id() {
        return *_id;
    }

private:
    PageId *_id;
};

class FileFreeIndex: public BTree {
public:
    FileFreeIndex(): BTree(10,false) {

    }

    PageId getPageWithSpace(MemSize size);
private:
    PageLink _rootLink;
    

};


#endif