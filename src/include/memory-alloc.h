#ifndef _MALLOC_H_
#define _MALLOC_H_
#include <stdlib.h>
#include "typedefs.h"
#include <stdexcept>
#include <vector>




template<class T>
class MemoryIterator;

template<class T>
class Memory {
    friend MemoryIterator<T>;
public:
    Memory() : _buf(NULL), _size(0), _link(false) {

    }
    Memory(MemSize s) : _buf(NULL), _size(0), _link(false) {
        alloc(s);
    }

    Memory(void *abuf,MemSize asize) : _buf(NULL), _size(0), _link(false) {
        alloc(asize);
        byte *to=(byte *)_buf;
        byte *from=(byte*)abuf;
        for(MemSize i=0;i<_size;i++) {
            to[i]=from[i];
        }

    }
    ~Memory(){
        clear();
    }

    void clear() {
        if(_buf!=NULL && !_link) {
            free(_buf);
        }
        _buf=NULL;
        _link=false;
        _size=0;
    }

    virtual bool linkTo(void *buf) {
        void *lastBuf(_buf);
        _buf=buf;
        if(lastBuf!=NULL && _size>0) {

            set(0,lastBuf,_size);
            free(lastBuf);

        }
        _link=true;
        return true;
    }

    bool set(MemSize pos,T val) {
        if(pos<_size) {
            ((T*)_buf)[pos]=val;
            return true;
        }
        return false;
    }

    template<class R>
    bool set(MemSize start, const R val) {
        return set(start,&val,sizeof(R));
    }

    bool set(MemSize start,const void *from, MemSize len) {
        if(start+len<=_size) {
            unsigned char *toByte((unsigned char*)_buf+start);
            unsigned char *fromByte((unsigned char*)from);
            for(MemSize i=0;i<len;i++) {
                *toByte=*fromByte;
                toByte++;
                fromByte++;
            }
            return true;
        }
        return false;
    }
    

    MemoryIterator<T> iterator() {
        return MemoryIterator<T>(*this);
    }
    bool alloc(MemSize size) {
        if(_link) {
            throw std::invalid_argument("Cannot resize memori linked !");
        }
        _size=size;
        if(size>0) {
            if(_buf==NULL) {
                _buf=malloc(size*sizeof(T*));
            } else {
                _buf=realloc(_buf,size*sizeof(T*));
            }
        } else {
            if(_buf!=NULL) {
                free(_buf);
                _buf=NULL;
            }
        }
        return _buf!=NULL;
    }
    bool push(const void *buf, MemSize size) {
        
        if(alloc(_size+size)) {
            unsigned char *toByte((unsigned char*)_buf+(_size-size));
            unsigned char *fromByte((unsigned char*)buf);
            for(MemSize i=0;i<size;i++) {
                toByte[i]=fromByte[i];;
            }
            return true;

        }
        return false;

    }
    template<class R>
    bool push(const R &val) {
        MemSize s=sizeof(R);
        if(alloc(_size+s)) {
            unsigned char *to((unsigned char*)_buf+(_size-s));
            unsigned char *from((unsigned char*)&val);
            for(MemSize i=0;i<s;i++) {
                to[i]=from[i];;
            }
            return true;

        }
        return false;

    }
    template<class R>
    bool get(MemSize pos, R &val) {
        MemSize size=sizeof(R);
        if(pos+size<=_size) {
            unsigned char *to((unsigned char*)&val);
            unsigned char *from((unsigned char*)_buf+pos);
            for(MemSize i=0;i<size;i++) {
                to[i]=from[i];;
            }
            return true;
        }
        return false;
    }
    template<class R>
    const R get(MemSize pos) {
        R val;
        MemSize size=sizeof(R);
        if(pos+size<=_size) {
            unsigned char *to((unsigned char*)&val);
            unsigned char *from((unsigned char*)_buf+pos);
            for(MemSize i=0;i<size;i++) {
                to[i]=from[i];;
            }
        }
        return val;
    }

    bool copyTo(void *to,MemSize start, MemSize len) {
        if(start+len<=_size) {
            unsigned char *toByte((unsigned char*)to);
            unsigned char *fromByte((unsigned char*)_buf+start);
            for(MemSize i=0;i<len;i++) {
                toByte[i]=fromByte[i];
            }
            return true;
        }
        return false;
    }

    
    MemSize size() const {
        return _size;
    }
    T *buf() const {
        return (T*)_buf;
    }

    T operator[](MemSize pos) const {
        if(pos<_size) {
            return ((T*)_buf)[pos];
        }
        return NULL;
    }


protected:
    void *_buf;
    MemSize _size; 
    bool _link;

};


template<class T>
class MemoryIterator {
    public:
        MemoryIterator(Memory<T> &mem): _pos(0) {
            _parent=&mem;
        }
        template<class R>
        const R get() {
            R val(_parent->get<R>(_pos));
            _pos+=sizeof(R);
            return val;
        }
        bool get(void *dest, MemSize len) {
            bool ret=_parent->copyTo(dest,_pos,len);
            _pos+=len;
            return ret;
        }
        void *buf() {
            return _parent->buf()+_pos;
        }
        MemSize operator+=(MemSize step) {
            _pos+=step;
            return _pos;
        }
    private:
        Memory<T> *_parent;
        MemSize _pos;

};

typedef Memory<byte> ByteBuf;

#endif