#ifndef _FILE_STORAGE_TREE_H_
#define _FILE_STORAGE_TREE_H_
#include "pages-cache.h"
#include "storage-data-tree.h"
#include <map>


class FileStorageNode;

class FileStorageTree: public StorageBTree {
public: 
    FileStorageTree(StorageBTNode *root,StorageManager *manager,byte order);

    PageLink &rootLink();
    void rootLink(const PageLink &link);

    PageLink & pageLink();
    void pageLink(const PageLink &link);

    FileStorageNode *root();


    void rootUpdated();
    virtual bool serialize(ByteBuf &buf);
    virtual bool deserialize(MemoryIterator<byte> &buf);
    

    virtual BTNode *createNode( bool leaf,BTNode *from);
    FileStorageNode *getOrCreateNode( bool leaf, const PageLink &link);
    virtual FileStorageNode *createNode( bool leaf, const PageLink &link)=0;

    virtual bool retrieve(StorageBTNode **);

    FileStorageNode *retrieved(const PageLink &link);
    FileStorageNode *retrieved(FileStorageNode *node);
    void removeRetrieved(FileStorageNode *node);

protected:
    virtual BTNode *findleaf(const BTKey &key);



private:
    PageLink _rootLink;
    PageLink _pageLink;

    std::map<unsigned long long,FileStorageNode*> _retrieved;

};

class FileStorageNode: public StorageBTNode {
public:
    FileStorageNode(FileStorageTree *parent, byte order, bool leaf);
    FileStorageNode(FileStorageTree *parent, byte order,const PageLink &link);
    ~FileStorageNode();
    PageLink & pageLink();
    void pageLink(const PageLink &link);

    virtual bool serialize(ByteBuf &buf);
    virtual bool deserialize(MemoryIterator<byte> &buf);

    bool toLoad();
    void toLoad(bool val);

    virtual SerializableKey *newKey()=0;
    virtual SerializableVal *newVal()=0;

    BTNode *retrieveOrCached(BTNode *node);

    virtual BTNode *nodeParent();
    virtual BTNode *nodeLeft();
    virtual BTNode *nodeRight();
private:
    PageLink _pageLink;
    bool _toload;
};


#endif