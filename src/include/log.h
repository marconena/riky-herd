#ifndef _LOG_H_
#define _LOG_H_

#include <stdlib.h>
#include <vector>
#include <string>
#include <map>
#include <stack>
#include <chrono>
#include <iostream>


enum class LogLevel: short {
    LEVEL_NONE =0,
    LEVEL_TRACE ,
    LEVEL_DEBUG ,
    LEVEL_INFO,
    LEVEL_WARNING,
    LEVEL_ERROR
} ;


class LogAppender {
    protected:
        LogLevel _level;
        std::string _format;
        LogAppender(LogLevel level, std::string format): _level(level), _format(format) {
            if(_format.length()==0) {
                _format="%dT%t [%l] %c - %m";
            }
        };
    public:
        void log(LogLevel level, const std::string component, const std::string message);
        virtual void append(LogLevel level,const std::string message)=0;
};

class ConsoleAppender: public LogAppender {
    public:
    ConsoleAppender(LogLevel level): LogAppender(level,"%t [%l] %c %m") {

    }
    void append(LogLevel level,const std::string message);
};

class FileAppender: public LogAppender {
    public:
    FileAppender(std::string path, LogLevel level, const std::string format, bool truncate);
    
    void append(LogLevel level,const std::string message);
    private:
    std::string _path;
};


typedef struct {
    std::string name;
    uint64_t time;
} TransNameAndTime;

class Logger {
private:
    std::string _component;
    LogLevel _level;
public:
    Logger(const std::string component,LogLevel level);
    ~Logger();

    void log(LogLevel level, const std::string message);
    void logFormat(LogLevel level, const std::string format, ...);
    void logVFormat(LogLevel level, const std::string format, va_list args);

    bool enabled(LogLevel level);
    bool traceEnabled();
    bool debugEnabled();
    bool infoEnabled();
    bool warnEnabled();
    bool errorEnabled();

    void trace(const std::string message);
    void traceFormat(const std::string format, ...);
    void debug(const std::string message);
    void debugFormat(const std::string format, ...);
    void info(const std::string message);
    void infoFormat(const std::string format, ...);
    void warn(const std::string message);
    void warnFormat(const std::string format, ...);
    void error(const std::string message);
    void errorFormat(const std::string format, ...);

};

class LoggerTransaction {

public: 
    LoggerTransaction(Logger &log) ;
    const std::string name() ;

    uint64_t elapsed() ;
    const std::string push(const std::string name) ;
    const std::string pushprefixed(const std::string name) ;
    void pushTrace(const std::string name) ;
    void pushDebug(const std::string name) ;
    void pushInfo(const std::string name) ;
    void pushWarn(const std::string name) ;
    void pushError(const std::string name) ;

    const std::string pop();
    const std::string popprefixed();
    void popTrace();
    void popDebug();
    void popInfo();
    void popWarn();
    void popError();

    
    const std::string toString() ;

    void trace(const std::string message);
    void traceFormat(const std::string format, ...);
    void debug(const std::string message);
    void debugFormat(const std::string format, ...);
    void info(const std::string message);
    void infoFormat(const std::string format, ...);
    void warn(const std::string message);
    void warnFormat(const std::string format, ...);
    void error(const std::string message);
    void errorFormat(const std::string format, ...);
private: 
    const std::string push(const std::string name, uint64_t val) ;
    const std::string prefix();
    const std::string prefix(size_t size);
    Logger *_log;
    uint64_t  _now;
    std::string _name;
    std::vector<TransNameAndTime> _stack;
    uint64_t now() ;
    TransNameAndTime popInternal() ;

};



class LoggerManager {
    public:

        static LoggerManager &instance() {
            static LoggerManager instance;
            instance.addAppender("console",new ConsoleAppender(LogLevel::LEVEL_TRACE));
            return instance;
        }

        static Logger& getLogger(const std::string component,LogLevel level);

        size_t addAppender(const std::string name,LogAppender *appender);

        void log(LogLevel level, const std::string component, const std::string message);
        void logFormat(LogLevel level, const std::string component, const std::string format, ...);
        void logVFormat(LogLevel level, const std::string component, const std::string format, va_list args);

        static const std::string printf(const std::string format, ...);
        static const std::string vprintf(const std::string format, va_list args);
        static const std::string getDate(tm newtime);
        static const std::string getTime(tm newtime);

    private:
        LoggerManager();
        ~LoggerManager();
        std::map<std::string,LogAppender*> _appenders;
    
};


#endif
