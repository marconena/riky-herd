#ifndef _BTREE_H_
#define _BTREE_H_
#include "memory-alloc.h"

class BTVal;

typedef int (*CmpFn)(const BTVal &, const BTVal &);


class BTVal: public ByteBuf {
    public:
    BTVal(const BTVal &val);
    BTVal(MemSize size);
    ~BTVal();
    bool set(const byte *abuf, MemSize asize);
    bool set(const BTVal &val);
    MemSize copy(byte *to, const byte*from, MemSize size);

    private:
    bool equals(const byte *abuf, MemSize alen);

};
int defaultCmpFN(const BTVal &a, const BTVal &b);

class BTKey: public BTVal {
    public:
    BTKey(MemSize size,CmpFn fn): BTVal(size), _cmp(fn) {

    };

    BTKey(MemSize size): BTVal(size), _cmp(defaultCmpFN) {

    };

    BTKey(const BTKey &val) : BTVal(val) {
        _cmp=val.cmpFn();
    }
    int cmp(const BTVal &val) const;
    CmpFn cmpFn() const;
    protected:
        CmpFn _cmp;
};
class BTree;

class BTNode {

public:
    friend BTree;
    BTNode(BTNode *parent, bool leaf);
    BTNode(byte order, bool leaf);
    ~BTNode();
    byte order();
    bool leaf();
    byte size();
    bool isEmpty();
    bool isFull();
    virtual bool find(const BTKey &k,byte *pos);
    BTNode *child(byte pos);
    BTVal *val(byte pos);
    BTKey *key(byte pos);
    bool child(byte pos, BTNode *node);
    bool key(byte pos, BTKey *key);
    bool val(byte pos, BTVal *key);

    virtual const BTKey first();
    bool put(BTNode *node,BTNode **root);
    virtual bool put(const BTKey &k, const BTVal &val,BTNode **root);
    virtual bool set(const BTKey &k, const BTVal &val);
    virtual bool del(const BTKey &k,BTNode **root);
    bool del(BTNode *node,BTNode **root);
    bool get(const BTKey &k, BTVal &val);

    bool split(BTNode **root);
    virtual bool join(BTNode **root);
    virtual bool joinRight(BTNode **root);
    bool keyUpdated(BTNode *node);

    BTNode *left();
    BTNode *right();
    BTNode *left(BTNode *n);
    BTNode *right(BTNode *n);
    BTNode *parent();
    bool dirty();
    byte midle();

    virtual BTKey *newKey();
    virtual BTVal *newVal();

    virtual BTNode *newNode(byte order, bool leaf,BTNode *from);

    // Events
    virtual void nodeCreated();
    virtual void nodeDeleted();
    virtual void nodeUpdated();

    virtual BTNode *nodeParent();
    virtual BTNode *nodeLeft();
    virtual BTNode *nodeRight();
    

private:

    BTNode *root();
    

protected:
    void init();

    void removeMe();
    byte _order;

    bool _dirty;
    bool _leaf;
    byte _size;
    BTNode *_parent;
    //BTNode *_left;
    //BTNode *_right;

    Memory<BTNode*> _childs;
    Memory<BTKey *> _keys;
    Memory<BTVal *> _vals;
};



class BTree {
    public:
        BTree(byte order);
        BTree(byte order, bool unique);
        BTree(ByteBuf &mem);
        ~BTree();

        BTNode *root();
        byte order();
        bool unique();

        bool put(const BTKey &key, const BTVal &val);
        bool set(const BTKey &key, const BTVal &val);
        bool del(const BTKey &key);
        bool get(const BTKey &key, BTVal &val);
        virtual void rootUpdated();
    protected:
        BTree(BTNode *root,bool unique);
        virtual BTNode *findleaf(const BTKey &key);
        BTNode *firstleaf();
        BTNode *lastleaf();

        BTNode *_root;
        byte _order;
        bool _unique;

};

typedef unsigned long ArrayId;




class BTNodeArray: public BTNode {
    public: 
        BTNodeArray(byte order, bool leaf);
        ~BTNodeArray();
        ArrayId nextId();
        BTNode *newNode(byte order, bool leaf,BTNode *from);
        bool put(const BTKey &k, const BTVal &val,BTNode **root);
        bool find(const BTKey &k,byte *pos);
        ArrayId id(byte pos);
        void updateKey(ArrayId);
        const BTKey first();
        bool del(const BTKey &k,BTNode **root);
        bool joinRight(BTNode **root);
        
    protected:
     ArrayId _id;
};

class BTreeArray: public BTree {
    public:
        BTreeArray(byte order);

        ArrayId put(const BTVal &val);
        bool insert(ArrayId pos,const BTVal &val);
        bool del(ArrayId pos);
        bool get(ArrayId pos,BTVal &val);
        ArrayId size();
    

};


#endif