#ifndef _FILE_INDEX_H_
#define _FILE_INDEX_H_
#include "jnumber.h"
#include "file-storage-tree.h"

class IndexIdentifier: public Number, public SerializableKey {
public:
    static int Cmp(const BTVal &, const BTVal &);

    IndexIdentifier() : SerializableKey(IndexIdentifier::Cmp), Number(0) {

    }

    IndexIdentifier(NumberValue value): SerializableKey(IndexIdentifier::Cmp), Number(value) {

    }

    MemSize size() {
        return Number::size();
    }

    byte *buf() {
        return Number::buf();
    }
};

class BTPageLinkVal: public PageLink, public SerializableVal {
public:
    BTPageLinkVal() {

    }

    MemSize size() {
        return PageLink::size();
    }

    byte *buf() {
        return PageLink::buf();
    }

};

class NodeIndex: public FileStorageNode {
public:
    NodeIndex(FileStorageTree *tree): FileStorageNode(tree,5,false) {

    }
    NodeIndex(FileStorageTree *tree,bool leaf): FileStorageNode(tree,5,leaf) {

    }
    
    virtual SerializableKey *newKey();
    virtual SerializableVal *newVal();

};

class FileIndex: public FileStorageTree {
public:
    FileIndex(StorageManager *manager): FileStorageTree(new NodeIndex(this), manager,5) {

    }

    virtual FileStorageNode *createNode(bool leaf,const PageLink &link);

    bool put(const IndexIdentifier &key, const BTPageLinkVal &val);
private:
    PageLink _rootLink;

};




#endif