#ifndef _MEMORY_PAGE_H_
#define _MEMORY_PAGE_H_

#include "typedefs.h"
#include "memory-alloc.h"

typedef short PageElementID;



class MemoryPage: public ByteBuf  {
    public:
        MemoryPage(MemSize size, byte type);
        ~MemoryPage();
        PageElementID put(void *data, MemSize size);
        PageElementID set(PageElementID id, void *data, MemSize size);
        MemSize get(PageElementID id, void *data);
        PageElementID del(PageElementID id);
        PageElementID len();
        MemSize freeLength();

        byte type() const;

        bool dirty();
        void dirty(bool d);

        short getSize(PageElementID pos, bool real);

    private:
        bool setMemory(MemSize pos, void *p, MemSize len);

        bool setSize(PageElementID id, short size);

        PageElementID getFreeId();


        
        MemSize _offset;
        bool _dirty;
        PageElementID *_len;
        byte *_type;


};

#endif