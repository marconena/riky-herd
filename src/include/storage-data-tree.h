#ifndef _STORAGE_DATA_TREE_
#define _STORAGE_DATA_TREE_
#include "btree.h"

typedef enum {
    UPDATE=0,
    CREATE=1,
    DELETE=2
} NodeStoreOperation;

class Serializable {
    public:
        virtual bool serialize(ByteBuf &b)=0;
        virtual bool deserialize(MemoryIterator<byte> &i)=0;
};

class SerializableKey: public BTKey, public Serializable {

    public:
        SerializableKey(CmpFn fn);
        virtual bool serialize(ByteBuf &b);
        virtual bool deserialize(MemoryIterator<byte> &i);
    

};

class SerializableVal: public BTVal, public Serializable {
public:
    SerializableVal();
    virtual bool serialize(ByteBuf &b);
    virtual bool deserialize(MemoryIterator<byte> &i);

};

class StorageBTNode;
class StorageBTree;

class StorageManager {
    public:
        virtual void onChanged(StorageBTNode *node, NodeStoreOperation oper)=0;
        virtual bool retrieve(StorageBTNode *node)=0;

};


class StorageBTree: public BTree, public Serializable {
public:
    StorageBTree(StorageBTNode *root,StorageManager *manager,byte order);
    virtual BTNode *createNode( bool leaf,BTNode *from)=0;
    
    virtual void onChanged(StorageBTNode *node, NodeStoreOperation oper);
    virtual bool serialize(ByteBuf &buf);
    virtual bool deserialize(MemoryIterator<byte> &iter);
    virtual void rootUpdated();
    StorageBTNode *root();
    bool dirty();
    void dirty(bool val);

    StorageManager *storageManager();

    virtual bool retrieve(StorageBTNode **);

private:
    StorageManager *_manager;
    bool _dirty;
};


class StorageBTNode: public BTNode, public Serializable {
    public:
        StorageBTNode(StorageBTree *parent,byte order, bool leaf);


        StorageBTree *parent();

        BTNode *newNode(byte order, bool leaf,BTNode *from);

        StorageBTNode *child(byte pos);
        SerializableVal *val(byte pos);
        SerializableKey *key(byte pos);
        
        bool child(byte pos, StorageBTNode *node);
        bool key(byte pos, SerializableKey *key);
        bool val(byte pos, SerializableVal *key);

        void nodeCreated();
        void nodeDeleted();
        void nodeUpdated();

        /*
            Virtuals implementations
        */
        virtual StorageBTNode * retrieve();

        virtual bool serialize(ByteBuf &buf);
        virtual bool deserialize(MemoryIterator<byte> &iter);

        virtual SerializableKey *newKey()=0;
        virtual SerializableVal *newVal()=0;


    protected:
        StorageBTree *_tree;

};

#endif