#ifndef _SYSTEM_H_
#define _SYSTEM_H_
#include <string>

class System {
public:
    static bool ExistFile(const std::string path);
    static bool DeleteFile(const std::string path);

};

#endif