#ifndef _PAGE_CACHE_H_
#define _PAGE_CACHE_H_
#include "memory-page.h"
#include <map>
#include <vector>
#include "storage-data-tree.h"

#define PAGE_CACHE_SIZE 4096

typedef unsigned long PageId;

class PageCache: public MemoryPage {
public:
    PageCache(const PageCache &page);
    PageCache(byte type);
    PageId id() const {
        return _id;
    }
    void id(PageId id) {
        _id=id;
    }
    PageElementID push(const ByteBuf &buf) {
        return MemoryPage::put(buf.buf(),buf.size()); 
    }
    

private:
    PageId _id;
    

};

class PageLink: public ByteBuf {
public:
    PageLink(PageId apage, PageElementID aele): ByteBuf((MemSize)(sizeof(PageId) + sizeof(PageElementID))) {
        setRefs();
        page(apage);
        element(aele);
    };
    PageLink(): ByteBuf((MemSize)(sizeof(PageId) + sizeof(PageElementID))) {
        setRefs();
        page(0);
        element(0);
    };



    ~PageLink() {
        ByteBuf::clear();
    }


    bool linkTo(void *buf) {
        bool ret= ByteBuf::linkTo(buf);
        setRefs();

        return ret;
    }



    void operator=(const PageLink &link) {
        page(link.page());
        element(link.element());
    }

    bool operator!=(const PageLink &link) {
        return (*_page)!=link.page() || (*_element)!=link.element();
    }

    bool operator==(const PageLink &link) {
        return (*_page)==link.page() && (*_element)==link.element();
    }

    PageId page() const{
        return *_page;
    }
    void page(PageId val){
        *_page=val;
    }
    PageElementID element()  const {
        return *_element;
    }
    void element(PageElementID val)  {
        *_element=val;
    }
    bool good()  const{
        return (*_page)>0 && (*_element)>0;
    }
private:
    PageId *_page; 
    PageElementID *_element;

    void setRefs() {
        _page=(PageId*)buf();
        _element=(PageElementID*)(buf()+sizeof(PageId));
    }

};

class PageRetriever {
    public:
        virtual PageCache *getPage(PageId id)=0;
};

class PagesCache {
public:
    PagesCache(int maxSize, PageId pageCount,PageRetriever *retriever);
    ~PagesCache();

    PageCache *newPage(byte type, PageId id);

    bool push(const ByteBuf &buf, PageLink &link);
    bool pushToNew(const ByteBuf &buf, PageLink &link);

    bool del(const PageLink &link);

    int size();

    PageId getPageWithSpace(MemSize size);

    std::vector<PageCache*> getDirtyPages();

    bool dirty();
    void dirty(bool adirty);

    bool get(const PageLink &link,ByteBuf &buf);


    

    

private:
    std::map<PageId,PageCache*> _cache;
    bool _dirty;
    int _size;
    int _maxsize;
    PageId _pageCount;
    PageRetriever *_retriever;

    PageCache *page(PageId id);

};


class StorableDataTree: public StorageBTree {
public:
    StorableDataTree(StorageManager *manager, byte order );
    bool serialize(ByteBuf &buf);

    BTNode *createNode(byte order, bool leaf,BTNode *from);
    void rootUpdated();
    
private:
    PageLink _rootLink;

};

class StorableDataNode: public StorageBTNode {
    public:
        StorableDataNode(StorageBTree *parent, byte order, bool leaf): StorageBTNode(parent,order,leaf) {

        };
        PageLink pageLink();
        void pageLink(PageLink link);

    private:
        PageLink _pageLink;
    
};


#endif