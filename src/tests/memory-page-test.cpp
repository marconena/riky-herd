#include "log.h"
#include "memory-page.h"
#include "string.h"

void initPage(MemoryPage &page) {
    const char *str[]={"Marco Franceschin","Luca Giacomini","Matteo Boldrin","James Bound","Topolino è uns €", NULL};
    PageElementID id(0);

    const char **p(str);
    while((id=page.put((void*)*p,(MemSize)strlen(*p)))>0) {
        p++;
        if(*p==NULL) {
            p=str;
        }
    }
}

void testMemoryPage() {
    Logger log=LoggerManager::getLogger("testMemoryPage",LogLevel::LEVEL_TRACE);
    log.info("testMemoryPage: START...");
    LoggerTransaction trans(log);
    MemoryPage page(4096,0);
    trans.infoFormat("Page: size=%i, free=%i",page.len(),page.freeLength());
    PageElementID id(0);
    trans.pushInfo("PUT");
    initPage(page);
    trans.infoFormat("SIZE: %i",page.len());
    trans.popInfo();
    trans.pushInfo("DEL-FROM-LAST");
    PageElementID size(page.len());
    for(PageElementID i=size;i>0;i--) {
        page.del(i);
        //trans.infoFormat("SIZE: %i",page.len());
    }
    trans.infoFormat("SIZE: %i",page.len());
    trans.popInfo();
    initPage(page);
    trans.pushInfo("DEL-FROM-FIRST");
    size=page.len();
    for(PageElementID i=1;i<=size;i++) {
        page.del(i);
        //trans.infoFormat("SIZE: %i",page.len());
    }
    trans.infoFormat("SIZE: %i",page.len());
    trans.popInfo();

    initPage(page);
    trans.pushInfo("GET");
    char buf[100];
    size=page.len();
    for(PageElementID i=1;i<=size;i++) {
        MemSize s(page.get(i,buf));
        buf[s]='\0';
        // if(i%21==0) {

        //   trans.infoFormat("GET: %i: %s",i,buf);
        // }
    }
    trans.infoFormat("SIZE: %i",page.len());
    trans.popInfo();




    log.info("testMemoryPage: ...END");

}