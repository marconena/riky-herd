#include "log.h"
#include "btree.h"
#include "jnumber.h"
#include <cmath>
#include <iostream>
#include <iomanip>


class BTIdentifier : public BTKey
{

public:
    BTIdentifier(NumberValue id) : BTKey(0, (CmpFn)Number::cmp)
    {
        Number v(id);
        alloc(v.size());
        ByteBuf::set(0,v.buf(),v.size());
    }
    
private:
    
};

typedef unsigned int PageId;
typedef short PageIdx;

class PageLink: public BTVal {
    public:
        PageLink(PageId id, PageIdx idx): BTVal(sizeof(PageId)+sizeof(PageIdx)) {
            _id=(PageId *)buf();
            _idx=(PageIdx *)(buf()+sizeof(PageId));
            *_id=id;
            *_idx=idx;
        }
        bool empty() {
            return *_id == 0;
        }
    private:
        PageId *_id;
        PageIdx *_idx;

};

class BTreeIndex: public BTree {

public:
    BTreeIndex():BTree(5,false) {

    }
    NumberValue next() {
        NumberValue ret(1);
        BTNode *n(lastleaf());
        if(n->size()>0) {
            ret=Number::from(n->key(n->size()-1));
            ret++;
        }

        return ret;
    }

};

class FreeKey: public BTKey {
private:
    static int cmp(const BTVal &a, const BTVal &b) {
        return 0;
    }
    
public:
    FreeKey(MemSize size): BTKey(sizeof(MemSize),(CmpFn)FreeKey::cmp) {
        ByteBuf::set<MemSize>(0,size);
    }
};

class BTreeFree: public BTree {
public:
    BTreeFree(): BTree(5,false) {

    }

};

void testBTStoreTests()
{
    Logger log = LoggerManager::getLogger("testMemoryPage", LogLevel::LEVEL_TRACE);

    LoggerTransaction trans(log);
    trans.pushInfo("testBTStoreTests");
    trans.pushInfo("NumberValueentifier Extens");
    char buf[32];
    static const char hex[] = "0123456789ABCDEF";
    for (int i = 0; i < 61; i++)
    {
        NumberValue idbt((NumberValue)pow(2, i));
        BTIdentifier id(idbt);
        trans.debugFormat("id=%llu (%ibit)", idbt, i + 1);
        idbt = Number::from(&id);

        for (int i = 0; i < id.size(); i++)
        {
            byte c(id.buf()[i]);
            buf[i * 2 + 1] = hex[c & 0b1111];
            buf[i * 2] = hex[(c >> 4) & 0b1111];
        }
        buf[id.size() * 2] = '\0';

        trans.debugFormat("id=%llu (size=%i) 0x%s", idbt, id.size(), buf);
    }
    byte b[]={255,255,255,255,255,255,255,255};
    ByteBuf idbuf(b,8);
    trans.infoFormat("MAX Value is %llu", Number::from(&idbuf));

    trans.popInfo();
    trans.pushInfo("BTreeIndexes");
    BTreeIndex tree;
    for(int i=0;i<64000;i++) {
        tree.put(BTIdentifier(tree.next()),PageLink(0,0));
    }

    trans.popInfo();

    

    trans.popInfo();
}