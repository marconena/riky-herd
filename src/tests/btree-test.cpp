#include "log.h"
#include "btree.h"
#include <iostream>
#include <sstream>

Logger log1 = LoggerManager::getLogger("CMP",LogLevel::LEVEL_TRACE);

int keyCmp(const BTVal &v1, const BTVal &v2) {
    if(*((long long*)v1.buf())>*((long long*)v2.buf())) {
        //log1.debugFormat("KEYCMP: %li>%li (+1)",*((long long*)v1.buf()),*((long long*)v2.buf()));
        return 1;
    }
    if(*((long long*)v1.buf())<*((long long*)v2.buf())) {
        //log1.debugFormat("KEYCMP: %li<%li (-1)",*((long long*)v1.buf()),*((long long*)v2.buf()));
        return -1;
    }
    //log1.debugFormat("KEYCMP: %li=%li (0)",*((long long*)v1.buf()),*((long long*)v2.buf()));
    return 0;
}

class Key : public BTKey
{
public:
    Key(long long key) : BTKey(sizeof(long long),keyCmp) {
        _key=(long long *)buf();
        *_key=key;
    }

    long long val() const
    {
        return *_key;
    }

private:
    long long *_key;
};

class Val : public BTVal
{
public:
    Val(int val) : BTVal(sizeof(int))
    {
        _val=(int*)buf();
        *_val=val;
    }

    int val() {
        return *_val;
    }
    
    

private:
    int *_val;
};

class IDNode : public BTNode
{
public:
    IDNode(bool leaf) : BTNode(5, leaf)
    {
    }
    ~IDNode()
    {
    }
};

void dump(BTNode *node,Logger &log, int level) {

    std::ostringstream out;
    for(int i=0;i<level;i++) {
        out<<"  ";
    }
    if(level==0) {
        out<<"(ROOT) ";
    }
    out<<(int)node->size()<<"/"<<(int)node->order()<<(node->leaf() ? "L<":" <")<<node<<">[";
    if(node->leaf()) {

        for(byte i=0;i<node->size();i++) {
            if(i>0) {
                out<<", ";
            }
            //out<<(int)i<<":";
            out<<"("<<*((long long*)node->key(i)->buf())<<","<<*((int*)node->val(i)->buf())<<")";
        }
    } else {
        for(byte i=0;i<node->size();i++) {
            if(i>0) {
                out<<", ";
            }
            if(node->key(i)==NULL) {
                log.error("UNDEFINED!!!!");
            }
            //out<<(int)i<<":";
            out<<"("<<*((long long*)node->key(i)->buf())<<",";
            out<<node->child(i);
            out<<")";
        }
    }
    out<<"]";
    out<<"<<"<<node->left()<<"^^"<<node->parent()<<">>"<<node->right();
    
    log.info(out.str());
    if(!node->leaf()) {
        for(byte i=0;i<node->size();i++) {
            dump(node->child(i),log,level+1);
        }
    }
}
void testBTree()
{
    int iterations=32000;
    Logger log = LoggerManager::getLogger("BTree",LogLevel::LEVEL_TRACE);
    log.infoFormat("testBTree: iterations: %i",iterations);
    LoggerTransaction trans(log);
    log.info(trans.push("btree-test"));
    BTree tree(5);

    log.info(trans.push("PUT"));
    for(int i=0;i<iterations;i++) {
        bool ret(tree.put(Key(i),Val(i)));
        //log.infoFormat("+: (%li,%i) %s",i,i, ret ? "OK": "KO");
    }
    log.info(trans.pop());
    // for(int i=0;i<iterations;i++) {
    //     log.infoFormat("-%li: %s",i, tree.del(Key(i)) ? "OK": "KO");
    //     dump(tree.root(), log,0);
    // }
    // dump(tree.root(), log,0);
    log.info("+: (5,3200)");
    tree.put(Key(5),Val(32000));
    log.info("+: (5,3201)");

    tree.put(Key(5),Val(32001));
    // Qui
    log.info("+: (5,3202)");
    tree.put(Key(5),Val(32002));
    log.info("+: (5,3203)");
    tree.put(Key(5),Val(32003));
    log.info("+: (5,3204)");
    tree.put(Key(5),Val(32004));
    tree.put(Key(5),Val(32005));
    for(int i=0;i<8;i++) {
        log.infoFormat("%i:DEL 5: %s",i,tree.del(Key(5)) ? "DELETED": "NOT DELETED");
    }
    log.infoFormat("DEL 9: %s",tree.del(Key(9)) ? "DELETED": "NOT DELETED");
    log.info(trans.push("SET"));
    for(int i=0;i<iterations;i++) {
        bool ret(tree.set(Key(i),Val(iterations-(i+1))));
        //log.infoFormat("SET: %li=%i %s",i,iterations-(i+1),ret ? "OK": "KO");
    }
    log.info(trans.pop());

    log.info(trans.push("GET"));
    for(int i=0;i<iterations;i++) {
        Val val(0);
        bool ret(tree.get(Key(i),val));
        // if(ret) {
        //     log.infoFormat("GET: %li=%i",i,val.val());
        // } else {
        //     log.infoFormat("GET: %li NOT FOUND",i);
        // }
    }
    log.info(trans.pop());
    
    log.info(trans.push("DEL"));
    for(int i=5;i<iterations;i++) {
        bool ret(tree.del(Key(i)));
        //log.infoFormat("-: %li: %s",i,ret ? "DELETED":"NOT DELETED");
    }
    log.info(trans.pop());
    dump(tree.root(), log,0);

    /*
    */
    log.info(trans.toString());    
    
}

void dumpArray(BTNode *node, LoggerTransaction &trans) {
    BTNodeArray *n((BTNodeArray*)node);
    std::ostringstream out;
    out<<(node->parent() ? "<": "(root)<")<<node<<(node->leaf() ? ">L(":">(")<<(int)node->size()<<"/"<<(int)node->order()<<")[";
    for(byte i=0;i<node->size();i++) {
        if(i>0) out<<", ";
        out<<"("<<n->id(i)<<",";
        if(node->leaf()) {
            MemoryIterator<byte> i1(node->val(i)->iterator());
            out<<i1.get<int>();
        } else {
            out<<node->child(i);
        }
        out<<")";
    }
    out<<"] (<"<<n->left()<<"^"<<n->parent()<<">"<<n->right();
    trans.debug(out.str());
    if(!node->leaf()) {
        trans.push("");
        for(byte i=0;i<node->size();i++) {
            dumpArray(node->child(i), trans);
        }
        trans.pop();

    }


}

void testBTreeArray() {
    int cnt(32000);
    int ins(500);
    Logger log = LoggerManager::getLogger("BTreeArray",LogLevel::LEVEL_TRACE);
    LoggerTransaction trans(log);
    trans.pushInfo("testBTreeArray");
    BTreeArray array(5);
    trans.pushInfo("PUT");
    for(int i=0;i<cnt;i++) {
        //trans.infoFormat("+%i:%i",i,cnt-i);
        array.put(Val(cnt-i));
    }
    trans.popInfo();
    //dumpArray(array.root(),trans);
    trans.pushInfo("INSERT-FIRST");
    for(int i=0;i<ins;i++) {
        array.insert(0,Val(20000+i));
        //dumpArray(array.root(),trans);
    }
    trans.popInfo();
    //dumpArray(array.root(),trans);
    trans.pushInfo("INSERT-LAST");
    ArrayId size=array.size();
    for(int i=0;i<ins;i++) {
        array.insert(size+ins+1,Val(10000+i));
    }
    trans.popInfo();
    //dumpArray(array.root(),trans);
    size=array.size();
    trans.infoFormat("Array size %li",size);
    Val val(0);
    trans.pushInfo("GET");
    for(ArrayId i=0;i<size+2;i++) {
        array.get(i,val);
    }
    trans.popInfo();
    //dumpArray(array.root(),trans);
    trans.pushInfo("DEL-FIRST");
    for(ArrayId i=0;i<size;i++) {
        ArrayId pos(array.size());
        if(i%1000==0) trans.infoFormat("-: %li",pos);
        array.del(0);
        //dumpArray(array.root(),trans);
        
        
    }
    trans.popInfo();
    dumpArray(array.root(),trans);
    
    
    //dumpArray(array.root(),trans);

    trans.popInfo();

}


