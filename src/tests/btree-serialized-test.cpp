#include "log.h"
#include "btree.h"
#include <iostream>
#include <sstream>

typedef unsigned long long KeyType;

int keyIdCmp(const BTVal &a, const BTVal &b) {
    KeyType ak(*((KeyType*)a.buf()));
    KeyType bk(*((KeyType*)b.buf()));
    if(ak>bk) return 1;
    if(ak<bk) return -1;
    return 0;
}

class KeyIDTest: public BTKey {
    public:
        KeyIDTest(KeyType val): BTKey(sizeof(KeyType), keyIdCmp) {
            _val=(KeyType*)buf();
            *_val=val;
        }

        KeyType val() {
            return *_val;
        }

    private:

    KeyType *_val;
};

class PageLinkTest: public BTVal {
    public:
        PageLinkTest(unsigned long page, short pos):BTVal(sizeof(unsigned long)+sizeof(short)) {
            _page=(unsigned long *)buf();
            _pos=(short *)(buf()+sizeof(unsigned long));
            *_page=page;
            *_pos=pos;
        }
    private:
        unsigned long *_page;
        short *_pos;
};

void dumpNode(BTNode *node) {
    Logger log=LoggerManager::getLogger("DumpNode",LogLevel::LEVEL_TRACE);
    log.infoFormat("%s(%i/%i)",node->leaf() ? "LEAF":"",node->size(),node->order());
    if(node->size()>0) {
        std::ostringstream out;
        out<<"   Keys[";
        for(byte i=0;i<node->size();i++) {
            if(i>0) {
                out<<", ";
            }
            out<<*((KeyType*)node->key(i)->buf());
        }
        out<<" ]";
        log.info(out.str());
        if(node->leaf()) {
            std::ostringstream out;
            out<<"   Values[";
            for(byte i=0;i<node->size();i++) {
                if(i>0) {
                    out<<", ";
                }
                out<<*((unsigned long*)node->val(i)->buf())<<":"<<*((short*)(node->val(i)->buf()+sizeof(unsigned long)));
            }
            out<<" ]";
            log.info(out.str());

        }
    }

}


void testBTreeSerialize() {
    int iterations(24);
    Logger log=LoggerManager::getLogger("BTreeSerialize",LogLevel::LEVEL_TRACE);
    LoggerTransaction trans(log);
    log.info(trans.push("testBTreeSerialize"));
    
    
    log.info(trans.pop());

}