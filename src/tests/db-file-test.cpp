#include "log.h"
#include "db-file.h"
#include "file-index.h"
#include "system.h"
#include <fstream>

void bigDB(LoggerTransaction &trans, long long max)
{
    trans.pushInfo("testBigDB");
    trans.infoFormat("SIZE LIMIT %lli",max);
    std::string file("test-bid-db.rh");
    if(true) {
        trans.pushInfo("CREATE");
        if (!System::DeleteFile(file))
        {
            trans.errorFormat("Error on deleting: %s", file.c_str());
        }
        else
        {
            trans.infoFormat("Deleted file: %s", file.c_str());
        }    
        DBFile db(file);
        if (db.openOrCreate())
        {
            trans.infoFormat("Created DB: %s",file.c_str());
            std::ifstream fdata;
            fdata.open("../../J4SHOPRUNTIME.ITEMS",std::ios::in);
            if(fdata.is_open()) {
                char c;
                std::string str;
                long long idx(0);
                while(fdata.get(c)) {
                    if(c=='\n' || c=='\r') {
                        if(str.length()>0) {
                            size_t pos=str.find("|");
                            if(pos>0) {
                                std::string id(str.substr(0,pos));
                                std::string val(str.substr(pos+1));
                                //printf("%s=%s\n",id.c_str(),val.c_str());
                                if(!db.push(id,val)) {

                                    trans.warnFormat("Val %s NOT ADDED !",id.c_str());
                                }
                                idx++;
                                if(id.compare("MARCO")==0) {
                                    trans.infoFormat("MARCO FOUND on %lli: %s",idx,val.c_str());
                                }
                                if((idx%1000)==0) {
                                    trans.infoFormat("+%lli",idx);
                                    db.commit();
                                }
                                if(idx>=max) {
                                    break;
                                }
                            }
                            str="";
                        }
                    } else {
                        str+=c;
                    }

                }
                fdata.close();
            } else {
                trans.error("Cannot open text file !");
            }
            db.commit();
            db.close();
            trans.popInfo();

        }
    }
    if(true) {
        trans.pushInfo("READ");
        DBFile db(file);
        if (db.openOrCreate())
        {
            trans.infoFormat("OPENED DB: %s",file.c_str());
            std::ifstream fdata;
            fdata.open("../../J4SHOPRUNTIME.ITEMS",std::ios::in);
            if(fdata.is_open()) {
                char c;
                std::string str;
                long long idx(0);
                while(fdata.get(c)) {
                    if(c=='\n' || c=='\r') {
                        if(str.length()>0) {
                            size_t pos=str.find("|");
                            if(pos>0) {
                                idx++;
                                StringKey id(str.substr(0,pos).c_str());
                                StringVal val;
                                //printf("%s=%s\n",id.c_str(),val.c_str());
                                if(!db.get(id,val)) {
                                    trans.warnFormat("%lli: %s not found !",idx,id.value().c_str());
                                }
                                
                                if((idx%1000)==0) {
                                    trans.infoFormat("+%lli: readeds.",idx);
                                }
                                if(idx>=max) {
                                    break;
                                }
                            }
                            str="";
                        }
                    } else {
                        str+=c;
                    }

                }
                fdata.close();
            } else {
                trans.error("Cannot open text file !");
            }
            db.close();
            trans.popInfo();

        }
    }

    trans.popInfo();
}


void shortTest(LoggerTransaction &trans) {
    if (true)
    {

        std::string file("test-db.rh");

        // Create test
        if (true)
        {
            trans.pushInfo("CREATE");
            if (!System::DeleteFile(file))
            {
                trans.errorFormat("Error on deleting: %s", file.c_str());
            }
            else
            {
                trans.infoFormat("Deleted file: %s", file.c_str());
            }

            DBFile db(file);
            if (db.openOrCreate())
            {

                trans.info("Database is created !");
                db.push("001", "Marco Franceschin");

                db.push("003", "Ricky Franceschin 3");
                db.push("004", "Ricky Franceschin 4");
                db.push("005", "Ricky Franceschin 5");
                db.push("002", "Alice Bado");

                db.push("006", "Ricky Franceschin 6");
                db.push("007", "Ricky Franceschin 7");
                db.push("008", "Ricky Franceschin 8");
                db.push("009", "Ricky Franceschin 9");
                db.push("010", "Ricky Franceschin 10");

                db.commit();
                // Add too mani articles

                db.close();
            }
            else
            {
                trans.error("Error on create dbatabase file !");
            }
            trans.popInfo();
        }

        // Get test
        if (true)
        {

            // Now try to open the file
            trans.pushInfo("LOAD");
            DBFile db1(file);
            if (db1.openOrCreate())
            {
                char *keys[] = {"002", "010", "006", "005", "NON ESISTE", "008", "001", "003", NULL};
                trans.info("Database is open !");
                StringVal val;
                char **p(keys);
                while (*p)
                {
                    if (db1.get(*p, val))
                    {
                        trans.infoFormat("Find \"%s\"=\"%s\"", *p, val.value().c_str());
                    }
                    else
                    {
                        trans.warnFormat("NOT found \"%s\" !", *p);
                    }
                    ++p;
                }

                db1.close();
            }
            else
            {
                trans.error("Error on open dbatabase file !");
            }
            trans.popInfo();
        }
        // Delete test
        if (true)
        {
            trans.pushInfo("DELETE");
            DBFile db2(file);
            if (db2.openOrCreate())
            {
                char *keys[] = {"005", "001", "002", "003", "NON ESISTE", "008", "010", "001", "004", "005", NULL};
                trans.info("Database is open !");
                StringVal val;
                char **p(keys);
                while (*p)
                {

                    if (db2.del(*p))
                    {
                        trans.infoFormat("Deleted element \"%s\"", *p);
                    }
                    else
                    {
                        trans.warnFormat("NOT found \"%s\" !", *p);
                    }
                    ++p;
                }

                db2.commit();
                db2.close();
            }
            else
            {
                trans.error("Error on open dbatabase file !");
            }
            trans.popInfo();
        }
    }

}

void testDbFile()
{
    Logger log = LoggerManager::getLogger("testDbFile", LogLevel::LEVEL_TRACE);
    LoggerTransaction trans(log);
    trans.pushInfo("testDbFile");
    //shortTest(trans);
    bigDB(trans, 100);
    trans.popInfo();
}