#ifndef _tests_h_
#define _tests_h_

void testJsonVal();

void testJsonObject();

void testBPlusNodeArray();

void testStream();


void testBTree();

void testBTreeSerialize();

void testBTreeArray();

void testMemoryPage();

void testBTStoreTests();

void testDbFile();

#endif