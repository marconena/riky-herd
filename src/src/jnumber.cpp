#include "jnumber.h"

Number::Number(NumberValue id) : ByteBuf((MemSize)0)
{
    alloc(sizeOf(id));
    byte *b(buf());
    byte *from((byte *)&id);
    for (byte i = 0; i < size(); i++)
    {
        byte val(from[size() - i - 1]);
        if (i == 0)
        {
            val |= ((size() - 1) << 5);
        }
        b[i] = val;
    }
}
NumberValue Number::from(const ByteBuf *buf)
{
    byte len((buf->buf()[0] >> 5) + 1);
    NumberValue ret(0);
    for (int i = 0; i < len; i++)
    {
        byte c(buf->buf()[i]);
        if (i == 0)
            c &= 0b00011111;
        ((byte *)&ret)[len - i - 1] = c;
    }

    return ret;
}

char Number::sizeOf(NumberValue id)
{
    byte ret = 1;
    while (id >> 8 || (id & 0b11100000))
    {
        ret++;
        id = id >> 8;
    }
    return ret;
}

int Number::cmp(const ByteBuf &a, const ByteBuf &b) {
    NumberValue a0(from(&a));
    NumberValue b0(from(&b));
    if(a0>b0) return 1;
    if(a0<b0) return -1;

    return 0;
}

