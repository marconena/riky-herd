#include "file-index.h"

int IndexIdentifier::Cmp(const BTVal &, const BTVal &) {
    return 0;
}

bool FileIndex::put(const IndexIdentifier &key, const BTPageLinkVal &val) {
    return BTree::put(key,val);
}

FileStorageNode *FileIndex::createNode(bool leaf,const PageLink &link) {
    FileStorageNode *ret(new NodeIndex(this,leaf));

    if(link.good()) {
        ret->pageLink(link);
        ret->toLoad(true);
    }
    return ret;

}

SerializableKey *NodeIndex::newKey() {
    return new IndexIdentifier();
}
SerializableVal *NodeIndex::newVal() {
    return new BTPageLinkVal();
}
