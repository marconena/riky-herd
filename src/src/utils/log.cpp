#include <chrono>
#include <ctime>
#include <iomanip>
#include <sstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "log.h"
#include <vector>
#include <string>
#include <iostream>

/**
 * @brief Construct a new Logger:: Logger object
 * 
 */
LoggerManager::LoggerManager() {
	
}

LoggerManager::~LoggerManager() {
	for(std::map<std::string,LogAppender*>::iterator i=_appenders.begin();i!=_appenders.end();i++) {
		delete (*i).second;
	}
	_appenders.clear();
}

size_t LoggerManager::addAppender(const std::string name,LogAppender *appender) {
	_appenders.insert(std::pair<std::string,LogAppender*>(name,appender));
	return _appenders.size();
}


const std::string LoggerManager::getTime(tm newtime) {
	using namespace std::chrono;

	// get current time
    auto now = system_clock::now();

    // get number of milliseconds for the current second
    // (remainder after division into seconds)
    auto ms = duration_cast<milliseconds>(now.time_since_epoch()) % 1000;
    
	return LoggerManager::printf("%02i:%02i:%02i.%03i",
		newtime.tm_hour,
		newtime.tm_min,
		newtime.tm_sec,
		ms.count()
		);
}

const std::string LoggerManager::getDate(tm newtime) {
	return LoggerManager::printf("%04i-%02i-%02i",
		newtime.tm_year+1900, 
		newtime.tm_mon+1,
		newtime.tm_mday );
}

const std::string LoggerManager::vprintf(const std::string format, va_list args) {
	int size=vsnprintf(NULL,0,format.c_str(),args);
	char *msg= new char[size+1];
	vsnprintf(msg,size+1,format.c_str(),args);
	std::string ret(msg);
	delete msg;
	return ret;
}
const std::string LoggerManager::printf(const std::string format, ...) {
	va_list args;
    va_start(args, format);
	std::string ret=LoggerManager::vprintf(format,args);
	va_end(args);
	return ret;
}

void LoggerManager::logFormat(LogLevel level, const std::string component, const std::string format, ...) {

	va_list args;
    va_start(args, format);
	
	va_end(args);

	
}

void LoggerManager::logVFormat(LogLevel level, const std::string component, const std::string format,  va_list args)
{

	std::string message=LoggerManager::vprintf(format,args);
	log(level,component,message);

}

void LoggerManager::log(LogLevel level, const std::string component, const std::string message) {
	// Log to all appenders
	for(std::map<std::string,LogAppender*>::iterator i=_appenders.begin();i!=_appenders.end();i++) {
		(*i).second->log(level,component,message);
	}

}

Logger& LoggerManager::getLogger(const std::string component,LogLevel level) {
	std::string comp=component.size()==0 ? "root" : component;
	return *(new Logger(comp,level));
}

Logger::Logger(const std::string component,LogLevel level): _component(component), _level(level) {

}
Logger::~Logger() {
}

void Logger::log(LogLevel level, const std::string message) {
	LoggerManager::instance().log(level,_component,message);

}
void Logger::logFormat(LogLevel level, const std::string format, ...) {
	va_list args;
    va_start(args, format);
	LoggerManager::instance().log(level,_component,args);
	va_end(args);
}

void Logger::logVFormat(LogLevel level, const std::string format, va_list args) {
	LoggerManager::instance().logVFormat(level,_component,format,args);
	
}

bool Logger::enabled(LogLevel level) {
	return level>=_level;
}
bool Logger::traceEnabled() {
	return enabled(LogLevel::LEVEL_TRACE);
}
bool Logger::debugEnabled() {
	return enabled(LogLevel::LEVEL_DEBUG);
}
bool Logger::infoEnabled() {
	return enabled(LogLevel::LEVEL_INFO);
}
bool Logger::warnEnabled() {
	return enabled(LogLevel::LEVEL_WARNING);
}
bool Logger::errorEnabled()  {
	return enabled(LogLevel::LEVEL_ERROR);
}

void Logger::trace(const std::string message) {
	if(!traceEnabled()) return;
	LoggerManager::instance().log(LogLevel::LEVEL_TRACE,_component,message);
}
void Logger::traceFormat(const std::string format, ...) {
	if(!traceEnabled()) return;
	va_list args;
	va_start(args, format);
	LoggerManager::instance().logVFormat(LogLevel::LEVEL_TRACE,_component,format,args);
	va_end(args);

}
void Logger::debug(const std::string message) {
	if(!debugEnabled()) return;
	LoggerManager::instance().log(LogLevel::LEVEL_DEBUG,_component,message);
}
void Logger::debugFormat(const std::string format, ...) {
	if(!debugEnabled()) return;
	va_list args;
	va_start(args, format);
	LoggerManager::instance().logVFormat(LogLevel::LEVEL_DEBUG,_component,format,args);
	va_end(args);
}
void Logger::info(const std::string message) {
	if(!infoEnabled()) return;

	LoggerManager::instance().log(LogLevel::LEVEL_INFO,_component,message);
}
void Logger::infoFormat(const std::string format, ...) {
	if(!infoEnabled()) return;
	va_list args;
	va_start(args, format);
	LoggerManager::instance().logVFormat(LogLevel::LEVEL_INFO,_component,format,args);
	va_end(args);
}
void Logger::warn(const std::string message) {
	if(!warnEnabled()) return;
	LoggerManager::instance().log(LogLevel::LEVEL_WARNING,_component,message);
}
void Logger::warnFormat(const std::string format, ...) {
	if(!warnEnabled()) return;
	va_list args;
	va_start(args, format);
	LoggerManager::instance().logVFormat(LogLevel::LEVEL_WARNING,_component,format,args);
	va_end(args);
}
void Logger::error(const std::string message) {
	if(!errorEnabled()) return;
	LoggerManager::instance().log(LogLevel::LEVEL_ERROR,_component,message);
}
void Logger::errorFormat(const std::string format, ...) {
	if(!errorEnabled()) return;
	va_list args;
	va_start(args, format);
	LoggerManager::instance().logVFormat(LogLevel::LEVEL_ERROR,_component,format,args);
	va_end(args);
}

void LogAppender::log(LogLevel level, const std::string component, const std::string message) {
	// Execute log only the level is greather or equals at appender log level
	if(level>=_level) {
		__time64_t long_time;
    	// Get time as 64-bit integer.
    	_time64( &long_time );
		tm newtime;
    
    	errno_t err;

    	// Convert to local time.
    	err = _localtime64_s( &newtime, &long_time );

		std::ostringstream out;
		for(std::string::iterator i=_format.begin();i!=_format.end();i++) {
			if(*i=='%') {
				i++;
				switch(*i) {
					case 'm': // Message
						out<<message;
						break;
					case 'c': // Message
						out<<component;
						break;
					case 'l': // level
						switch(level) {
							case LogLevel::LEVEL_TRACE:
								out<<"TRACE";
								break;
							case LogLevel::LEVEL_DEBUG:
								out<<"DEBUG";
								break;
							case LogLevel::LEVEL_INFO:
								out<<"INFO";
								break;
							case LogLevel::LEVEL_WARNING:
								out<<"WARN";
								break;
							case LogLevel::LEVEL_ERROR:
								out<<"ERROR";
								break;
						}
						break;
					case 't': // time
						out<<LoggerManager::getTime(newtime);
						break;
					case 'd': // date
						out<<LoggerManager::getDate(newtime);
						break;
					case '%': // date
						out<<'%';
						break;
					default:
						out<<'%'<<*i;

				}
			} else {
				out<<*i;
			}
		}

		append(level, out.str());

	}
}

void ConsoleAppender::append(LogLevel level, const std::string message) {
	if(level==LogLevel::LEVEL_ERROR) {
		std::cerr<<message<<"\n";
	} else {
		std::cout<<message<<"\n";
	}
}

FileAppender::FileAppender(std::string path, LogLevel level, const std::string format, bool truncate): LogAppender(level,format), _path(path) {
	if(truncate) {
		FILE *f=fopen(path.c_str(),"w");
		if(f!=NULL) {
			fclose(f);
		}

	}
}

void FileAppender::append(LogLevel level,const std::string message) {
	FILE *f;
	if(!fopen_s(&f,_path.c_str(),"a+")) {
		fputs(message.c_str(),f);
		fputc('\n',f);
		fclose(f);
	}

}


LoggerTransaction::LoggerTransaction(Logger &log): _log(&log) {

}

const std::string LoggerTransaction::name() {
	return _name;
}

uint64_t LoggerTransaction::elapsed() {
	return now() - _now;
}
const std::string LoggerTransaction::push(const std::string name) {
	return push(name, now());
}
const std::string LoggerTransaction::push(const std::string name, uint64_t val)  {
	_stack.push_back({name,val});
	std::ostringstream out;
	out<<"> "<<name;
	return out.str();
}
const std::string LoggerTransaction::pushprefixed(const std::string name) {
	std::ostringstream out;
	std::string msg(push(name));
	out<<prefix(_stack.size()-1);
	out<<msg;
	return out.str();
}
void LoggerTransaction::pushTrace(const std::string name) {
	if(!_log->traceEnabled()) {
		push(name);
		return;
	}
	_log->trace(pushprefixed(name));
}
void LoggerTransaction::pushDebug(const std::string name) {
	if(!_log->debugEnabled()) {
		push(name);
		return;
	}
	_log->debug(pushprefixed(name));

}
void LoggerTransaction::pushInfo(const std::string name) {
	if(!_log->infoEnabled()) {
		push(name);
		return;
	}
	_log->info(pushprefixed(name));

}
void LoggerTransaction::pushWarn(const std::string name) {
	if(!_log->warnEnabled()) {
		push(name);
		return;
	}
	_log->warn(pushprefixed(name));

}
void LoggerTransaction::pushError(const std::string name) {
	if(!_log->errorEnabled()) {
		push(name);
		return;
	}
	_log->error(pushprefixed(name));
}

const std::string LoggerTransaction::pop() {
	std::ostringstream out;
	if(!_stack.empty()) {
		TransNameAndTime t=popInternal();
		out<<"< "<<t.name<<": "<<now() - t.time<<"ms";
	}

	return out.str();
}

const std::string LoggerTransaction::popprefixed() {
	std::ostringstream out;
	std::string msg(pop());
	out<<prefix();
	out<<msg;
	return out.str();
}

void LoggerTransaction::popTrace() {
	if(!_log->traceEnabled()) {
		pop();
		return;
	}
	_log->trace(popprefixed());

}
void LoggerTransaction::popDebug() {
	if(!_log->debugEnabled()) {
		pop();
		return;
	}
	_log->debug(popprefixed());

}
void LoggerTransaction::popInfo() {
	if(!_log->infoEnabled()) {
		pop();
		return;
	}
	_log->info(popprefixed());
}
void LoggerTransaction::popWarn() {
	if(!_log->warnEnabled()) {
		pop();
		return;
	}
	_log->warn(popprefixed());

}
void LoggerTransaction::popError() {
	if(!_log->errorEnabled()) {
		pop();
		return;
	}
	_log->error(popprefixed());

}

const std::string LoggerTransaction::toString() {
	std::ostringstream out;

	for(int i=(int)_stack.size()-1;i>=0;i--) {
		TransNameAndTime t=_stack.at(i);
		if(out.str().length()>0) {
			out <<", ";
		}
		out<<t.name<<": "<<now() - t.time<<"ms";
	}
	

	return out.str();
}

uint64_t LoggerTransaction::now() {
        return std::chrono::duration_cast< std::chrono::milliseconds >(
		std::chrono::system_clock::now().time_since_epoch()
	).count();
}

TransNameAndTime LoggerTransaction::popInternal()  {
	TransNameAndTime ret={"",0};
	if(!_stack.empty()) {
		ret=_stack.at(_stack.size()-1);
		_stack.pop_back();
		return ret;
	}
	return ret;
}
const std::string LoggerTransaction::prefix(size_t size) {
	std::ostringstream out;
	for(size_t i=0;i<size;i++) {
		out<<". ";
	}
	return out.str();
}
const std::string LoggerTransaction::prefix() {
	return prefix(_stack.size());
}

void LoggerTransaction::trace(const std::string message) {
	if(!_log->traceEnabled()) {
		return;
	}
	std::ostringstream out;
	out<<prefix();
	out<<message;
	_log->trace(out.str());
}
void LoggerTransaction::traceFormat(const std::string format, ...) {
	if(!_log->traceEnabled()) {
		return;
	}
	std::ostringstream out;
	out<<prefix();
	va_list args;
	va_start(args, format);
	out<<LoggerManager::vprintf(format,args);
	va_end(args);
	_log->trace(out.str());
}
void LoggerTransaction::debug(const std::string message)
{
	if(!_log->debugEnabled()) {
		return;
	}
	std::ostringstream out;
	out<<prefix();
	out<<message;
	_log->debug(out.str());

}
void LoggerTransaction::debugFormat(const std::string format, ...) {
	if(!_log->debugEnabled()) {
		return;
	}
	std::ostringstream out;
	out<<prefix();
	va_list args;
	va_start(args, format);
	out<<LoggerManager::vprintf(format,args);
	va_end(args);
	_log->debug(out.str());
}
void LoggerTransaction::info(const std::string message) {
	if(!_log->infoEnabled()) {
		return;
	}
	std::ostringstream out;
	out<<prefix();
	out<<message;
	_log->info(out.str());
}
void LoggerTransaction::infoFormat(const std::string format, ...) {
	if(!_log->infoEnabled()) {
		return;
	}
	std::ostringstream out;
	out<<prefix();
	va_list args;
	va_start(args, format);
	out<<LoggerManager::vprintf(format,args);
	va_end(args);
	_log->info(out.str());

}
void LoggerTransaction::warn(const std::string message) {
	if(!_log->warnEnabled()) {
		return;
	}
	std::ostringstream out;
	out<<prefix();
	out<<message;
	_log->warn(out.str());
}
void LoggerTransaction::warnFormat(const std::string format, ...) {
	if(!_log->warnEnabled()) {
		return;
	}
	std::ostringstream out;
	out<<prefix();
	va_list args;
	va_start(args, format);
	out<<LoggerManager::vprintf(format,args);
	va_end(args);
	_log->warn(out.str());
}
void LoggerTransaction::error(const std::string message) {
	if(!_log->errorEnabled()) {
		return;
	}
	std::ostringstream out;
	out<<prefix();
	out<<message;
	_log->error(out.str());
}
void LoggerTransaction::errorFormat(const std::string format, ...) {
	if(!_log->errorEnabled()) {
		return;
	}
	std::ostringstream out;
	out<<prefix();
	va_list args;
	va_start(args, format);
	out<<LoggerManager::vprintf(format,args);
	va_end(args);
	_log->error(out.str());

}