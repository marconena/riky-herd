#include "system.h"

#include <sys/stat.h>
#include <fstream>
#include <stdio.h>


bool System::ExistFile(const std::string path) {
    std::ifstream f(path.c_str());
    return f.good();
}

bool System::DeleteFile(const std::string path) {
    if(ExistFile(path)) {
        return remove(path.c_str())==0;
    }
    return false;
}