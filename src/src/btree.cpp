#include "btree.h"
#include <stdlib.h>
#include "log.h"

Logger btLog = LoggerManager::getLogger("BTree", LogLevel::LEVEL_TRACE);

BTVal::BTVal(const BTVal &val) : ByteBuf(val.buf(), val.size())
{
    set(val.buf(), val.size());
}

BTVal::BTVal(MemSize size) : ByteBuf(size)
{
}

BTVal::~BTVal()
{
}

MemSize BTVal::copy(byte *to, const byte *from, MemSize size)
{

    for (MemSize i = 0; i < size; i++)
    {
        to[i] = from[i];
    }
    return size;
}

bool BTVal::set(const BTVal &val)
{
    return set(val.buf(), val.size());
}

bool BTVal::set(const byte *abuf, MemSize asize)
{

    if (!equals(abuf, asize))
    {
        alloc(asize);
        ByteBuf::set(0, abuf, asize);
        return true;
    }

    return false;
}

bool BTVal::equals(const byte *abuf, MemSize alen)
{
    if (alen == size())
    {
        for (MemSize i = 0; i < alen; i++)
        {
            if (abuf[i] != (*this)[i])
            {
                return false;
            }
        }
        return true;
    }

    return false;
}

int BTKey::cmp(const BTVal &val) const
{
    if (_cmp != NULL)
    {
        return _cmp(*this, val);
    }
    return 0;
}

CmpFn BTKey::cmpFn() const
{
    return _cmp;
}

BTNode::BTNode(BTNode *parent, bool leaf) : _parent(parent), _order(parent->order()), _leaf(leaf), _size(0),  _dirty(false)
{
    init();
}

BTNode::BTNode(byte order, bool leaf) : _order(order), _leaf(leaf), _size(0), _parent(NULL),  _dirty(false)
{
    init();
}

void BTNode::init()
{
    _keys.alloc(_order);
    if (_leaf)
    {
        _childs.alloc(0);
        _vals.alloc(_order);
    }
    else
    {
        _childs.alloc(_order);
        _vals.alloc(0);
    }
    for (byte i = 0; i < _order; i++)
    {
        _keys.set(i, NULL);
        if (_leaf)
        {
            _vals.set(i, NULL);
        }
        else
        {
            _childs.set(i, NULL);
        }
    }
}

BTNode::~BTNode()
{
    printf("~BTNode: %i (%s) %p\n",_size, _leaf ? "leaf": "node", this);
    for (byte i = 0; i < _size; i++)
    {
        if (_keys[i])
            delete _keys[i];
    }
    if (_leaf)
    {
        for (byte i = 0; i < _size; i++)
        {
            if (_vals[i])
                delete _vals[i];
        }
    }
    else
    {
        for (byte i = 0; i < _size; i++)
        {
            if (_childs[i])
                delete _childs[i];
        }
    }
}
bool BTNode::dirty()
{
    return _dirty;
}
byte BTNode::order()
{
    return _order;
}

bool BTNode::leaf()
{
    return _leaf;
}

byte BTNode::size()
{
    return _size;
}

byte BTNode::midle()
{

    return _order / 2 + (_order % 2);
}

bool BTNode::find(const BTKey &k, byte *pos)
{
    *pos = 0;
    int cmp = 0;

    while (*pos < _size && (cmp = k.cmp(*_keys[*pos])) > 0)
    {
        (*pos)++;
    }

    if (*pos < _size)
    {
        return cmp == 0;
    }
    return false;
}

bool BTNode::isEmpty()
{
    return _size == 0;
}
bool BTNode::isFull()
{
    return _size >= _order;
}

BTNode *BTNode::child(byte pos)
{
    if (!_leaf && pos < _size)
    {
        return _childs[pos];
    }
    return NULL;
}

BTVal *BTNode::val(byte pos)
{
    if (_leaf && pos < _size)
    {
        return _vals[pos];
    }
    return NULL;
}
BTKey *BTNode::key(byte pos)
{
    return _keys[pos];
}

bool BTNode::child(byte pos, BTNode *node) {
    if (pos < _size)
    {
        return _childs.set(pos, node);
    }
    return false;
}

bool BTNode::key(byte pos, BTKey *key)
{
    if (pos < _size)
    {
        return _keys.set(pos, key);
    }
    return false;
}

bool BTNode::val(byte pos, BTVal *key)
{
    if (pos < _size)
    {
        return _vals.set(pos, key);
    }
    return false;
}



bool BTNode::put(const BTKey &k, const BTVal &val, BTNode **root)
{
    if (_leaf && !isFull())
    {
        byte pos;
        find(k, &pos);
        for (byte i = _size; i > pos; i--)
        {
            _keys.set(i, _keys[i - 1]);
            _vals.set(i, _vals[i - 1]);
        }
        BTKey *nk=newKey();
        nk->set(k);

        _keys.set(pos, nk);
        BTVal *nv=newVal();
        nv->set(val);
        _vals.set(pos, nv);
        _size++;
        if (pos == 0 && nodeParent())
        {
            _parent->keyUpdated(this);
        }
        if(!split(root)) {
            nodeUpdated();
        }
        _dirty = true;
        return true;
    }
    return false;
}

bool BTNode::keyUpdated(BTNode *node)
{
    if (!_leaf && node->_size > 0)
    {
        for (byte i = 0; i < _size; i++)
        {
            if (_childs[i] == node)
            {
                if (_keys[i]->set(node->first()))
                {
                    _dirty = true;
                    if (i == 0 && nodeParent())
                    {
                        _parent->keyUpdated(this);
                    }
                    nodeUpdated();
                    return true;
                } 
            }
        }
    }
    return false;
}

bool BTNode::put(BTNode *node, BTNode **root)
{
    if (!_leaf && !isFull() && node->size() > 0)
    {
        byte pos;
        find(node->first(), &pos);
        // move on right
        for (byte i = _size; i > pos; i--)
        {
            _childs.set(i, _childs[i - 1]);
            _keys.set(i, _keys[i - 1]);
        }
        BTKey *nk=newKey();
        nk->set(node->first());
        _keys.set(pos, nk);
        _childs.set(pos, node);
        _size++;
        node->_parent = this;
        if (pos == 0 && _parent)
        {
            nodeParent()->keyUpdated(this);
        }
        if(!split(root)) {
            nodeUpdated();
        }
        _dirty = true;
        return true;
    }
    return false;
}

void BTNode::removeMe() {
    _size=0;
    nodeDeleted();
    delete this;


}

bool BTNode::del(BTNode *node, BTNode **root)
{
    if (!_leaf)
    {
        for (byte i = 0; i < _size; i++)
        {
            if (node == _childs[i])
            {
                delete _keys[i];
                for (byte k = i; k < _size - 1; k++)
                {
                    _keys.set(k, _keys[k + 1]);
                    _childs.set(k, _childs[k + 1]);
                }
                _size--;
                _dirty = true;
                if (i == 0 && _parent && _size)
                {
                    nodeParent()->keyUpdated(this);
                }
                if(!_size && _parent) {
                    nodeParent()->del(this,root);
                    removeMe();

                } else if (!join(root) )
                {
                    if(!_parent && _size == 1) {

                        _childs[0]->_parent = NULL;
                        *root = _childs[0];
                        // Is root with 1 value
                        delete _keys[0];
                        _size = 0;
                        nodeDeleted();
                        delete this;
                    } else {
                        nodeUpdated();
                    }
                }

                return true;
            }
        }
    }
    return false;
}

BTNode *BTNode::root()
{
    BTNode *ret(this);
    while (ret->_parent)
    {
        ret = ret->_parent;
    }

    return ret;
}
bool BTNode::set(const BTKey &k, const BTVal &val)
{
    byte pos;
    if (_leaf && find(k, &pos))
    {
        if (_vals[pos]->set(val))
        {
            _dirty = true;
            nodeUpdated();
        }
        return true;
    }
    return false;
}

bool BTNode::get(const BTKey &k, BTVal &val)
{
    byte pos;
    if (_leaf && find(k, &pos))
    {
        val.set(*_vals[pos]);
        return true;
    }
    return false;
}

bool BTNode::del(const BTKey &k, BTNode **root)
{
    byte pos;
    if (_leaf && find(k, &pos))
    {
        delete _vals[pos];
        delete _keys[pos];
        for (byte i = pos; i < _size - 1; i++)
        {
            _keys.set(i, _keys[i + 1]);
            _vals.set(i, _vals[i + 1]);
        }
        _size--;
        _dirty = true;
        if (_size && pos == 0 && _parent)
        {
            nodeParent()->keyUpdated(this);
        }
        if(!_size && _parent) {
            nodeParent()->del(this,root);
            removeMe();
        } else {
            if(!join(root)) {
                nodeUpdated();
            }
        }

        return true;
    }
    return false;
}

BTNode *BTNode::left(BTNode *n) {

    if(!leaf()) {
        for(byte i=0;i<size();i++) {
            if(child(i)==n) {
                return child(i-1);
            }
        }
    }
    return NULL;

}
BTNode *BTNode::left()
{
    if(parent()) {
        return parent()->left(this);
    }
    return NULL;
}
BTNode *BTNode::parent()
{
    return _parent;
}

BTNode *BTNode::right(BTNode *n) {
    if(!leaf()) {
        for(byte i=0;i<size();i++) {
            if(child(i)==n) {
                return child(i+1);
            }
        }

    }
    return NULL;
}
BTNode *BTNode::right()
{
    if(parent()) {
        return parent()->right(this);
    }
    return NULL;
}

BTree::BTree(byte order) : _order(order), _root(new BTNode(order, true)), _unique(false)
{
}
BTree::BTree(byte order, bool unique) : _order(order), _root(new BTNode(order, true)), _unique(unique)
{
}
BTree::BTree(ByteBuf &mem)
{
    MemoryIterator<byte> iter(mem.iterator());
    _order = iter.get<byte>();
    _unique = iter.get<bool>();
    _root = new BTNode(_order, true);
}

BTree::BTree(BTNode *root, bool unique) : _root(root), _unique(unique)
{
    _order = root->order();
}
BTree::~BTree()
{
    if (_root != NULL)
    {
        delete _root;
    }
}

BTNode *BTree::findleaf(const BTKey &key)
{

    BTNode *ret = _root;
    while (!ret->leaf())
    {
        byte pos;
        if (!ret->find(key, &pos) && pos > 0)
        {
            pos--;
        }

        ret = ret->child(pos);
    }

    return ret;
}

bool BTNode::split(BTNode **root)
{
    if (isFull())
    {
        //printf("SPLIT!!!\n");
        BTNode *ret = newNode(_order, _leaf, this);
        
        byte m=midle();
        for (byte i = midle(); i < _order; i++)
        {
            byte idx = i - midle();
            ret->_keys.set(idx, _keys[i]);
            if (_leaf)
            {
                ret->_vals.set(idx, _vals[i]);
            }
            else
            {
                ret->_childs.set(idx, _childs[i]);
                _childs[i]->_parent = ret;
            }
        }
        bool addParent = false;
        _size = midle();
        if (_parent == NULL)
        {
            addParent = true;
            _parent = newNode(_order, false, this);
            _parent->put(this, root);
            *root = _parent;

        }
        
        ret->_size = _order - midle();
        
        
        ret->_parent=_parent;
        ret->nodeCreated();
        bool b= _parent->put(ret, root) != NULL || addParent;
        nodeUpdated();

        return b;
    }
    return false;
}

BTNode *BTNode::newNode(byte order, bool leaf, BTNode *from)
{
    return new BTNode(order, leaf);
}

bool BTNode::joinRight(BTNode **root)
{
    BTNode *nRight(NULL);
    
    if (nodeRight() && ((right()->_size + _size) <= midle() || right()->_size == 0 || _size==0))
    {
        nRight = right();
        
    }
    if (nRight)
    {
        for (byte i = 0; i < nRight->_size; i++)
        {
            byte leftPos = this->_size + i;
            this->_keys.set(leftPos, nRight->_keys[i]);
            if (_leaf)
            {
                this->_vals.set(leftPos, nRight->_vals[i]);
            }
            else
            {
                this->_childs.set(leftPos, nRight->_childs[i]);
                if (this->_childs[leftPos])
                {
                    this->_childs[leftPos]->_parent = this;
                }
            }
        }
        this->_size += nRight->_size;
        if (this->_size == nRight->_size && this->_parent)
        {
            this->_parent->keyUpdated(this);
        }
        nRight->_size = 0;
        if (nRight->nodeParent())
        {
            nRight->_parent->del(nRight, root);
        }
        nodeUpdated();
        nRight->removeMe();
        printf("JOIN !!!\n");
        return true;
    }
    return false;
}

bool BTNode::join(BTNode **root)
{

    if (joinRight(root))
    {
        return true;
    }
    if (left())
    {
        return nodeLeft()->joinRight(root);
    }
    return false;
}

const BTKey BTNode::first()
{
    if (_size > 0)
    {
        return *_keys[0];
    }

    return BTKey(0, NULL);
}

void BTNode::nodeCreated() {

}
void BTNode::nodeDeleted() {

}
void BTNode::nodeUpdated() {

}

BTKey *BTNode::newKey() {
    return new BTKey(0,defaultCmpFN);
}
BTVal *BTNode::newVal() {
    return new BTVal(0);

}

BTNode *BTNode::nodeParent() {
    return _parent;
}
BTNode *BTNode::nodeLeft() {
    return left();
}
BTNode *BTNode::nodeRight() {
    return right();
}

bool BTree::put(const BTKey &key, const BTVal &val)
{
    BTNode *node = findleaf(key);
    BTNode *newRoot(NULL);
    byte pos;
    if (_unique && node->find(key, &pos))
    {
        return false;
    }
    bool ret = node->put(key, val, &newRoot);
    if (newRoot != NULL && newRoot != _root)
    {
        _root = newRoot;
        rootUpdated();
    }
    return ret;
}

void BTree::rootUpdated(){

}

bool BTree::set(const BTKey &key, const BTVal &val)
{
    BTNode *node = findleaf(key);
    return node->set(key, val);
}
bool BTree::del(const BTKey &key)
{
    BTNode *node = findleaf(key);
    BTNode *newRoot(NULL);

    bool ret = node->del(key, &newRoot);
    if (newRoot != NULL && newRoot != _root)
    {
        _root = newRoot;
    }

    return ret;
}

bool BTree::get(const BTKey &key, BTVal &val)
{
    BTNode *node = findleaf(key);
    return node->get(key, val);
}

BTNode *BTree::root()
{
    return _root;
}
byte BTree::order()
{
    return _order;
}
bool BTree::unique()
{
    return _unique;
}


BTNode *BTree::firstleaf()
{
    BTNode *n(_root);
    while (!n->leaf())
    {
        n = n->child(0);
    }

    return n;
}
BTNode *BTree::lastleaf()
{
    BTNode *n(_root);
    while (!n->leaf())
    {
        n = n->child(n->size() - 1);
    }
    return n;
}

/*****************************************
 * Array implementations
 * ****************************************/

int arrayCmp(const BTVal &a, const BTVal &b)
{
    ArrayId aid(*((ArrayId *)a.buf()));
    ArrayId bid(*((ArrayId *)b.buf()));
    if (aid > bid)
        return 1;
    if (aid < bid)
        return -1;
    return 0;
}

class ArrayKey : public BTKey
{
public:
    ArrayKey(ArrayId id) : BTKey(sizeof(ArrayId), arrayCmp)
    {
        _id = (ArrayId *)buf();
        *_id = id;
    }
    ArrayId id();
    static ArrayId id(const BTKey *key)
    {
        return *((ArrayId *)key->buf());
    }

private:
    ArrayId *_id;
};

BTNodeArray::BTNodeArray(byte order, bool leaf) : BTNode(order, leaf), _id(0)
{
}

BTNodeArray::~BTNodeArray()
{
}
BTNode *BTNodeArray::newNode(byte order, bool leaf, BTNode *from)
{
    BTNodeArray *n(new BTNodeArray(order, leaf));
    if (leaf)
    {
        
        n->_id = ((BTNodeArray *)from)->_id + midle();
    }
    return n;
}

ArrayId BTNodeArray::nextId()
{
    return _id + _size;
}

bool BTNodeArray::put(const BTKey &k, const BTVal &val, BTNode **root)
{
    if (_leaf && !isFull())
    {
        byte pos;
        find(k, &pos);

        for (byte i = _size; i > pos; i--)
        {
            _vals.set(i, _vals[i - 1]);
        }

        if (pos == 0)
        {
            _id = ArrayKey::id(&k);
        }
        _vals.set(pos, new BTVal(val));
        _size++;
        if (pos == 0 && _parent)
        {
            _parent->keyUpdated(this);
        }
        split(root);
        if (right())
        {
            ((BTNodeArray *)nodeRight())->updateKey(_id + _size);
        }
        _dirty = true;
        return true;
    }
    return false;
}

bool BTNodeArray::del(const BTKey &k, BTNode **root)
{
    byte pos;
    if (_leaf && find(k, &pos))
    {
        delete _vals[pos];
        for (byte i = pos; i < _size - 1; i++)
        {
            _vals.set(i, _vals[i + 1]);
        }
        _size--;
        _dirty = true;
        if(!_size && _parent) {
            _parent->del(this,root);
            if(right()) ((BTNodeArray*)nodeRight())->updateKey(nextId());
            removeMe();            
        } else {
            if(!join(root)) {
                if(right()) ((BTNodeArray*)nodeRight())->updateKey(nextId());
            }
        }
        return true;
    }
    return false;
}

void BTNodeArray::updateKey(ArrayId id0)
{
    if (_leaf)
    {
        _id = id0;
        if (_parent)
        {
            _parent->keyUpdated(this);
        }
        if (right())
        {
            ((BTNodeArray *)nodeRight())->updateKey(id(0) + _size);
        }
    }
}

bool BTNodeArray::find(const BTKey &k, byte *pos)
{
    if (_leaf)
    {
        *pos = 0;
        if (_size > 0)
        {
            ArrayId id0(id(0));
            ArrayId id1(ArrayKey::id(&k));
            if (id1 < id0 + _size)
            {
                *pos = (byte)(id1 - id0);
                return true;
            }
            else
            {
                *pos = _size;
            }
        }
        return false;
    }
    return BTNode::find(k, pos);
}

const BTKey BTNodeArray::first()
{
    if (_leaf)
    {
        return ArrayKey(_id);
    }
    return BTNode::first();
}

ArrayId BTNodeArray::id(byte pos)
{
    if (pos < _size)
    {
        if (_leaf)
        {
            return _id + pos;
        }
        return ArrayKey::id(key(pos));
    }
    return 0;
}
bool BTNodeArray::joinRight(BTNode **root)
{
    if (BTNode::joinRight(root))
    {
        if (right() && _leaf)
        {
            ((BTNodeArray *)nodeRight())->updateKey(_id + _size);
        }
        return true;
    }
    return false;
}

BTreeArray::BTreeArray(byte order) : BTree(new BTNodeArray(order, true), false)
{
}

ArrayId BTreeArray::put(const BTVal &val)
{
    BTNodeArray *node((BTNodeArray *)lastleaf());
    ArrayId id(node->nextId());
    if (!BTree::put(ArrayKey(id), val))
    {
        return 0;
    }

    return id;
}

bool BTreeArray::insert(ArrayId pos, const BTVal &val)
{
    if (!BTree::put(ArrayKey(pos), val))
    {
        return false;
    }

    return true;
}

bool BTreeArray::del(ArrayId pos)
{
    return BTree::del(ArrayKey(pos));
}
bool BTreeArray::get(ArrayId pos, BTVal &val)
{
    return BTree::get(ArrayKey(pos), val);
}
ArrayId BTreeArray::size()
{
    BTNodeArray *node((BTNodeArray *)lastleaf());
    return node->id(0) + node->size();
}



int defaultCmpFN(const BTVal &a, const BTVal &b) {
    if(a.size()>b.size()) {
        return 1;
    }
    if(a.size()<b.size()) {
        return -1;
    }
    byte *ab(a.buf());
    byte *bb(a.buf());
    for(MemSize i=0;i<a.size();i++) {
        if((*ab)>(*bb)) {
            return 1;
        }
        if((*ab)<(*bb)) {
            return -1;
        }
    }

    
    return 0;
};
