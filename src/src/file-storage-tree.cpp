#include "file-storage-tree.h"


FileStorageTree::FileStorageTree(StorageBTNode *r,StorageManager *manager,byte order): StorageBTree(r,manager,order) {
    root()->toLoad(true);
}

PageLink &FileStorageTree::rootLink() {
    return _rootLink;
}
void FileStorageTree::rootLink(const PageLink &link) {
    if(_rootLink!=link) {
        _rootLink=link;
        dirty(true);
    }
}

PageLink & FileStorageTree::pageLink() {
    return _pageLink;
}
void FileStorageTree::pageLink(const PageLink &link) {
    _pageLink=link;
}

FileStorageNode *FileStorageTree::root() {
    return (FileStorageNode *)_root;
}


void FileStorageTree::rootUpdated() {
    StorageBTree::rootUpdated();
    rootLink(((FileStorageNode*)_root)->pageLink());
    this->dirty(true);
}

bool FileStorageTree::serialize(ByteBuf &buf) {
    buf.push("T<",2);
    buf.push<byte>(order());
    buf.push<bool>(unique());
    buf.push(rootLink().buf(), rootLink().size());
    buf.push(">",1);
    return true;
}
bool FileStorageTree::deserialize(MemoryIterator<byte> &i) {
    i+=2;
    _order=i.get<byte>();
    _unique=i.get<bool>();
    i.get(rootLink().buf(), rootLink().size());
    return true;
}

BTNode *FileStorageTree::findleaf(const BTKey &key) {
    FileStorageNode *ret = root();
    ret->retrieve();
    while (!ret->leaf())
    {
        byte pos;
        if (!ret->find(key, &pos) && pos > 0)
        {
            pos--;
        }

        FileStorageNode *lleaf((FileStorageNode *)ret->child(pos));

        StorageBTNode *n(lleaf->retrieve());
        if(n!=lleaf) {
            ret->child(pos,n);
            lleaf=(FileStorageNode *)n;
        }
        ret = lleaf;
        
    }
    return ret;

}

FileStorageNode *FileStorageTree::getOrCreateNode( bool leaf, const PageLink &link) {
    FileStorageNode *ret(retrieved(link));
    if(ret) {
        return ret;
    }

    return createNode(leaf,link);

}

BTNode *FileStorageTree::createNode( bool leaf,BTNode *from) {
    PageLink l;
    FileStorageNode *node=createNode(leaf,l);

    return node;
}

unsigned long long link2long(const PageLink &link) {
    unsigned long long ret=0;
    if(link.good()) {
        ret=link.element();
        ret|=(link.page()<<(8*sizeof(PageElementID)));

    }
    return ret;
}

FileStorageNode *FileStorageTree::retrieved(const PageLink &link) {
    if(link.good()) {
        unsigned long long val=link2long(link);
        std::map<unsigned long long,FileStorageNode*>::iterator i(_retrieved.find(val));
        if(i!=_retrieved.end()) {
            return (*i).second;
        }
    }
    return NULL;
}

FileStorageNode *FileStorageTree::retrieved(FileStorageNode *node) {
    return retrieved(node->pageLink());
    
}

void FileStorageTree::removeRetrieved(FileStorageNode *node) {
    unsigned long long val=link2long(node->pageLink());
    std::map<unsigned long long,FileStorageNode*>::iterator i(_retrieved.find(val));
    if(i!=_retrieved.end()) {
        _retrieved.erase(i);
    }
}
bool FileStorageTree::retrieve(StorageBTNode **node) {
    FileStorageNode *n((FileStorageNode*)*node);
    if(n->toLoad() && n->pageLink().good()) {
        unsigned long long val=link2long(n->pageLink());
        std::map<unsigned long long,FileStorageNode*>::iterator i(_retrieved.find(val));
        if(i!=_retrieved.end()) {
            *node=(*i).second;

            return true;
        }

        bool ret(StorageBTree::retrieve(node));
        if(n->pageLink().good()) {
            _retrieved.insert({val,n});
        }
        return ret;
    }

    return false;

}





FileStorageNode::FileStorageNode(FileStorageTree *parent, byte order,bool leaf): StorageBTNode(parent,order,leaf), _pageLink(0,0), _toload(false) {
    //printf("NEW NODE!!!\n");
}

FileStorageNode::FileStorageNode(FileStorageTree *parent,byte order,const PageLink &link):  StorageBTNode(parent,order,false), _pageLink(link.page(),link.element()), _toload(true) {
    printf("NEW NODE (MUST LOAD)!!!\n");

}

FileStorageNode::~FileStorageNode() {
    printf("DESTRUCT NODE!!!\n");
    ((FileStorageTree*)_tree)->removeRetrieved(this);
}

bool FileStorageNode::toLoad() {
    return _toload;
}

void FileStorageNode::toLoad(bool val) {
    _toload=val;

}

PageLink & FileStorageNode::pageLink() {
    return _pageLink;
}
void FileStorageNode::pageLink(const PageLink &link) {
    if(_pageLink != link) {
        if(_pageLink.good()) {
            // Link updated !!!
            _pageLink = link;
            if(nodeParent()) {
                ((FileStorageNode*)_parent)->nodeUpdated();
            }
        } else {
            _pageLink = link;
        }
        _dirty=true;
    }
}

bool FileStorageNode::serialize(ByteBuf &buf) {
    if(leaf()) {
        buf.push("L<",2);

    } else {

        buf.push("N<",2);
    }
    StorageBTNode::serialize(buf);
    
    
    if(!leaf()) {
        for(byte i=0;i<size();++i) {
            FileStorageNode *n((FileStorageNode*)child(i));
            buf.push(n->pageLink().buf(),n->pageLink().size());
        }
    }
    
    FileStorageNode *node;
    node=(FileStorageNode *)BTNode::parent();
    
    if(node) {
        buf.push((byte)1);
        buf.push(node->pageLink().buf(),node->pageLink().size());
    } else {
        buf.push((byte)0);
    }
    buf.push(">",1);
    return true;
}

bool FileStorageNode::deserialize(MemoryIterator<byte> &iter) {
    iter+=2;
    StorageBTNode::deserialize(iter);
    
    if(!leaf()) {
        PageLink lnk;
        for(byte i=0;i<size();++i) {
            if(iter.get(lnk.buf(),lnk.size())) {
                FileStorageNode *n=((FileStorageTree*)parent())->createNode(false, lnk);
                n->_parent=this;
                child(i,n);
            }
        }
    }
    byte b=iter.get<byte>();
    PageLink l;
    
    if(b&0b1) {
        iter.get(l.buf(),l.size());
        if(l.good() && !_parent) {
            _parent=((FileStorageTree*)_tree)->getOrCreateNode(false,l);
        }
    } else {
        _parent=NULL;
    }

    return true;
}

BTNode *FileStorageNode::retrieveOrCached(BTNode *node) {
    if(node) {
        BTNode *n(((FileStorageTree*)_tree)->retrieved((FileStorageNode*)node));
        
        if(n) {
            if(n!=node) {
                FileStorageNode *n0((FileStorageNode*)node);
                if(n0->_parent) {
                    FileStorageNode *p((FileStorageNode *)n0->_parent);
                    for(byte i=0;i<p->size();i++) {
                        if(p->child(i)==node) {
                            p->child(i,n0);
                            break;
                        }
                    }
                }
                delete node;
            }
            return n;
        } else {
            ((FileStorageNode*)node)->retrieve();
        }
    }
    return node;
}

BTNode *FileStorageNode::nodeParent() {
    _parent=retrieveOrCached(_parent);
    
    return _parent;

}
BTNode *FileStorageNode::nodeLeft() {
    return retrieveOrCached(left());
}
BTNode *FileStorageNode::nodeRight() {
    return retrieveOrCached(right());

}