#include "pages-cache.h"



PageCache::PageCache(const PageCache &page): MemoryPage(PAGE_CACHE_SIZE,page.type()), _id(page.id()) {

}

PageCache::PageCache(byte type): MemoryPage(4096,type), _id(0) {

}


PagesCache::PagesCache(int maxSize, PageId pageCount, PageRetriever *retriever): _maxsize(maxSize), _size(0), _pageCount(pageCount), 
    _retriever(retriever) {

}

PagesCache::~PagesCache() {
    for(std::map<PageId,PageCache*>::iterator i=_cache.begin();i!=_cache.end(); i++) {
        delete (*i).second;
    }

}

PageCache *PagesCache::newPage(byte type, PageId id) {
    PageCache *ret=new PageCache(type);
    ret->id(id);
    _cache.insert({id,ret});
    return ret;
}

bool PagesCache::pushToNew(const ByteBuf &buf, PageLink &link) {
    PageId page(getPageWithSpace(buf.size()));
    // No pages with 
    if(page==0) {
        PageCache *page(newPage(0,++_pageCount));
        link.page(page->id());
        link.element(page->push(buf));
        if(link.element()>0) {
            _dirty=true;
            return true;
        }
    } else {
        std::map<PageId,PageCache*>::iterator i=_cache.find(page);
        if(i!=_cache.end()) {
            link.page(page);
            link.element(i->second->push(buf));
            if(link.element()>0) {
                _dirty=true;
                return true;
            }
        }
    }
    return false;

}

bool PagesCache::push(const ByteBuf &buf, PageLink &link) {

    if(!link.good()) {
        return pushToNew(buf,link);
    } else {
        std::map<PageId,PageCache*>::iterator i=_cache.find(link.page());
        if(i!=_cache.end()) {
            PageElementID newId(i->second->set(link.element(),buf.buf(), buf.size()));
            if(newId==link.element()) {
                _dirty=true;
                return true;
            } else {
                // No enough space remove and add to onother page
                //printf("NOt enough space!!!\n");
                i->second->del(link.element());
                return pushToNew(buf,link);

            }
        }

    }
    return false;
}

bool PagesCache::del(const PageLink &link) {
    if(link.good()) {
        PageCache *p=page(link.page());
        
        if(p) {
            if(p->del(link.element())) {
                _dirty=true;
                return true;
            }
        }
    }

    return false;

}

int PagesCache::size() {
    return _size;
}

PageId PagesCache::getPageWithSpace(MemSize size) {
    PageId ret(0);
    for(std::map<PageId,PageCache*>::iterator i=_cache.begin();i!=_cache.end();i++) {
        if((*i).second->freeLength()>=size) {
            return (*i).first;
        }
    }

    return ret;
}


bool PagesCache::dirty() {
    return _dirty;
}

void PagesCache::dirty(bool adirty) {
    _dirty=adirty;
}

std::vector<PageCache*> PagesCache::getDirtyPages() {
    std::vector<PageCache*> ret;
    for(std::map<PageId,PageCache*>::iterator i=_cache.begin();i!=_cache.end();i++) {
        if((*i).second->dirty()) {
            ret.push_back((*i).second);
        }
    }
    return ret;
}

bool PagesCache::get(const PageLink &link,ByteBuf &buf) {
    buf.clear();
    PageCache *p=page(link.page());
    if(p) {
        short size(p->getSize(link.element(),true));
        if(size>=0) {
            buf.alloc(size);
            return p->get(link.element(),buf.buf())>=0;
        }
    }
    return false;

}

/**
 * @brief Search page on cache and if not chached retrieve from retriever
 * 
 * @param id 
 * @return PageCache* 
 */
PageCache *PagesCache::page(PageId id) {
    std::map<PageId,PageCache*>::iterator i=_cache.find(id);
    if(i!=_cache.end()) {
        return (*i).second;
    }
   PageCache *ret(_retriever->getPage(id));
   if(ret) {
    _cache.insert({id,ret});
   }

   return ret;

}