#include "memory-page.h"
#include <stdlib.h>

MemoryPage::MemoryPage(MemSize size, byte type): ByteBuf(size), _dirty(false) {
    ByteBuf::set(0,type);
    _type=buf();
    _len=(PageElementID*)(buf()+1);
    *_len=0;
    _offset=1+sizeof(*_len);

}
MemoryPage::~MemoryPage() {
}
byte MemoryPage::type() const {
    return *_type;
}

bool MemoryPage::setMemory(MemSize pos, void *p, MemSize len) {
    return ByteBuf::set(pos,p,len);
}

bool MemoryPage::setSize(PageElementID id, short size) {
    if(id<=*_len && *_len>0) {
        return setMemory(_offset + (id-1)*sizeof(short),&size,sizeof(short));
    }
    return false;

}

/**
 * @brief Get the element size
 * 
 * @param id id of element
 * @param real get real size -1 if element is deleted
 * @return MemSize 
 */
short MemoryPage::getSize(PageElementID id, bool real) {
    if(id<=*_len && *_len>0) {
        short ret(0);
        
        MemSize pos=_offset + (id-1)*sizeof(MemSize);
        ret=ByteBuf::get<short>(pos);
        
        return ret<0 && !real ? 0 : ret;
    }
    return -1;

}

bool MemoryPage::dirty() {
    return _dirty;
}
void MemoryPage::dirty(bool d) {
    _dirty=d;
}

PageElementID MemoryPage::put(void *data, MemSize size) {
    PageElementID recId=getFreeId();
    if(recId>0) {
        return set(recId,data,size);
    }
    if(freeLength()>=sizeof(MemSize)+size) {
        (*_len)++;
        setSize(*_len,size);
        
        MemSize pos=_size;
        for(PageElementID i=1;i<=*_len;i++) {
            pos-=getSize(i,false);
        }
        setMemory(pos,data,size);
        _dirty=true;

        return *_len;
    }
    return -1;
}

PageElementID MemoryPage::set(PageElementID id, void *data, MemSize size) {
    MemSize ret=getSize(id,false);
    if(ret>=0) {
        int sizeDiff=(int)size-(int)ret;
        // Check there are requred expanded size
        if(sizeDiff>0 && freeLength()<sizeDiff) {
            return -1;
        }
        
        MemSize pos=_size;
        for(PageElementID i=1;i<=id;i++) {
            pos-=getSize(i,false);
        }
        if(sizeDiff!=0) {
            MemSize bottom=pos;
            for(PageElementID i=id+1; i<=*_len;i++) {
                bottom -=getSize(i,false);
            }
            if(sizeDiff>0) {
                // Is bigger move left
                byte *_buf=buf();

                for(int i=bottom;i<pos;i++) {
                    _buf[i - sizeDiff]=_buf[i];
                }
            } else {
                byte *_buf=buf();
                // if less move right
                for(int i=pos-1;i>=bottom;i--) {
                    _buf[i - sizeDiff]=_buf[i];
                }
            }
            pos -=sizeDiff;
        }
        setSize(id,size);
        setMemory(pos,data,size);
        _dirty=true;
        return id;

    }

    return -1;
}

PageElementID MemoryPage::del(PageElementID id) {
    if(id>0 && id<=*_len) {
        if(getSize(id,true)>=0) {
            set(id,"",0);
            setSize(id,-1);
            _dirty=true;
        }
        while((*_len) && getSize(*_len,true)<0) {
            (*_len)--;
        }

        return id;
    }
    return -1;

}

MemSize MemoryPage::get(PageElementID id, void *data) {
    MemSize ret=getSize(id,true);
    if(ret>=0 && data!=NULL) {
        MemSize pos=_size;
        for(PageElementID i=1;i<=id;i++) {
            pos-=getSize(i,false);
        }
        ByteBuf::copyTo(data,pos,ret);
        return ret;
    }
    

    return 0;
}

MemSize MemoryPage::freeLength() {
    MemSize ret=_size - _offset;
    bool haveFreeElement=false;
    for(PageElementID i=1;i<=*_len;i++) {
        ret -= sizeof(MemSize);
        short s(getSize(i,true));
        if(s<0) {
            haveFreeElement=true;
        } else {
            ret-=s;
        }
    }
    if(!haveFreeElement) {
        ret-=sizeof(MemSize);
    }
    return ret;
}


PageElementID MemoryPage::len() {
    return *_len;

}

PageElementID MemoryPage::getFreeId() {
    for(PageElementID i=1;i<=*_len;i++) {
        if(getSize(i,true)<0) {
            return i;
        }
    }
    return -1;

}