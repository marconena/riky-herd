#include "file-data.h"
#include <stdio.h>
#include <stdlib.h>


int StringKey::Cmp(const BTVal &a, const BTVal &b) {
    
    MemSize i(0);
    byte *ab(a.buf()), *bb(b.buf());
    for(;i<a.size()&&i<b.size();++i) {
        if(*ab > *bb) {
            return 1;
        } else if(*ab < *bb) {
            return -1;
        }
        
        ab++;
        bb++;
    }
    if(i<a.size()) {
        return 1;
    } else if(i<b.size()) {
        return -1;
    }

    return 0;
}

StringKey::StringKey() : SerializableKey(StringKey::Cmp)  {
}

StringKey::StringKey(void *val,MemSize size): SerializableKey(StringKey::Cmp)  {

        ByteBuf::push((byte*)val,size);

}
StringKey::StringKey(const char *val): SerializableKey(StringKey::Cmp) {
        ByteBuf::push(val,(MemSize)strlen(val));
}

const std::string StringKey::value() {
    if(size()>0) {
        std::string s((char*)buf());

        s.resize(size());
        return s;
    }
    return "";
}




StringVal::StringVal() {
}

StringVal::StringVal(const char *val) {
    ByteBuf::push(val,(MemSize)strlen(val));
}

StringVal::StringVal(void *val,MemSize size) {
    ByteBuf::push((byte*)val,size);
}

const std::string StringVal::value() {
    if(size()>0) {
        std::string s((char*)buf());

        s.resize(size());
        return s;
    }
    return "";
}

FileDataNode::FileDataNode(FileData *parent, bool leaf): FileStorageNode(parent,5,leaf) {
    
}

FileDataNode::FileDataNode(FileData *parent,const PageLink &link):  FileStorageNode(parent,5, false) {

}




SerializableKey *FileDataNode::newKey() {
    return new StringKey();
}
SerializableVal *FileDataNode::newVal() {
    return new StringVal();
}


FileData::FileData(StorageManager *manager): FileStorageTree(new FileDataNode(this,true), manager,5) {

}



bool FileData::put(const StringKey &key, const StringVal &val) {
    if(!BTree::put(key,val)) {
        return BTree::set(key,val);
    }
    return true;
}
bool FileData::del(const StringKey &key) {
    return BTree::del(key);
}

FileStorageNode *FileData::createNode(bool leaf, const PageLink &link) {
    FileStorageNode *ret=new FileDataNode(this, leaf);
    if(link.good()) {
        ret->pageLink(link);
        ret->toLoad(true);
    }

    return ret;
}

