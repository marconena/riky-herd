#include "storage-data-tree.h"


SerializableKey::SerializableKey(CmpFn fn): BTKey(0,fn) {

}

bool SerializableKey::serialize(ByteBuf &b) {
    b.push<MemSize>(size());
    b.push(buf(),size());
    return true;
}
bool SerializableKey::deserialize(MemoryIterator<byte> &i) {
    clear();
    MemSize s=i.get<MemSize>();
    alloc(s);
    i.get(buf(),s);
    return true;
}

SerializableVal::SerializableVal(): BTVal(0) {

}

bool SerializableVal::serialize(ByteBuf &b) {
    b.push<MemSize>(size());
    b.push(buf(),size());
    return true;
}
bool SerializableVal::deserialize(MemoryIterator<byte> &i) {
    clear();
    MemSize s=i.get<MemSize>();
    alloc(s);
    i.get(buf(),s);
    return true;
}


bool serialize(ByteBuf &buf);
bool deserialize(MemoryIterator<byte> &buf);


StorageBTree::StorageBTree(StorageBTNode *root,StorageManager *manager, byte order): BTree(root,true), _manager(manager), _dirty(false) {
}



void StorageBTree::onChanged(StorageBTNode *node, NodeStoreOperation oper) {
    _manager->onChanged(node,oper);
}

StorageManager *StorageBTree::storageManager() {
    return _manager;
}

StorageBTNode *StorageBTree::root() {
    return (StorageBTNode *)BTree::root();

}



bool StorageBTree::serialize(ByteBuf &buf) {
    buf.push<byte>(order());
    buf.push<bool>(unique());
    return true;
}
bool StorageBTree::deserialize(MemoryIterator<byte> &iter) {
    _order=iter.get<byte>();
    _unique=iter.get<bool>();

    return true;
}

void StorageBTree::rootUpdated() {
    _dirty=true;

}

bool StorageBTree::dirty() {
    return _dirty;
}
void StorageBTree::dirty(bool val) {
    _dirty=val;

}

bool StorageBTree::retrieve(StorageBTNode **node) {
    return _manager->retrieve(*node);
}


StorageBTNode::StorageBTNode(StorageBTree *parent,byte order, bool leaf): BTNode(order,leaf),_tree(parent) {

}

StorageBTree *StorageBTNode::parent() {
    return _tree;
}

BTNode *StorageBTNode::newNode(byte order, bool leaf,BTNode *from) {
    return _tree->createNode(leaf,from);
}
void StorageBTNode::nodeCreated() {
    _tree->onChanged(this,NodeStoreOperation::CREATE);
}
void StorageBTNode::nodeDeleted() {
    _tree->onChanged(this,NodeStoreOperation::DELETE);
}
void StorageBTNode::nodeUpdated() {
    _tree->onChanged(this,NodeStoreOperation::UPDATE);
}

StorageBTNode *StorageBTNode::child(byte pos) {
    return (StorageBTNode *)BTNode::child(pos);

}
SerializableVal *StorageBTNode::val(byte pos) {
    return (SerializableVal *)BTNode::val(pos);

}
SerializableKey *StorageBTNode::key(byte pos) {
    return (SerializableKey *)BTNode::key(pos);

}

bool StorageBTNode::child(byte pos, StorageBTNode *node) {
    return (SerializableVal *)BTNode::child(pos,node);
}
bool StorageBTNode::key(byte pos, SerializableKey *k) {
    return (SerializableVal *)BTNode::key(pos,k);

}
bool StorageBTNode::val(byte pos, SerializableVal *v) {
    return (SerializableVal *)BTNode::val(pos,v);
}

bool StorageBTNode::serialize(ByteBuf &buf) {
    //buf.push<byte>(order());
    buf.push<bool>(leaf());
    buf.push<byte>(size());
    for(byte i=0;i<size();++i) {
        SerializableKey *k=key(i);
        k->serialize(buf);
    }
    // Serialize add values for leaf
    if(leaf()) {
        for(byte i=0;i<size();++i) {
            val(i)->serialize(buf);
        }
    }
    return true;
}
bool StorageBTNode::deserialize(MemoryIterator<byte> &iter) {
    //_order=iter.get<byte>();
    _leaf=iter.get<bool>();
    init();
    _size=iter.get<byte>();
    for(byte i=0;i<size();++i) {
        SerializableKey *k=newKey();
        k->deserialize(iter);
        key(i,k);
    }
    if(leaf()) {
        for(byte i=0;i<size();++i) {
            SerializableVal *v=newVal();
            v->deserialize(iter);
            val(i, v);
        }
    }

    return true;
}

StorageBTNode * StorageBTNode::retrieve() {
    StorageBTNode *n(this);
    _tree->retrieve(&n);
    if(n!=this) {
        delete this;
        return n;
    }

    return this;

}