#include "log.h"
#include "memory-page.h"
#include "tests/tests.h"


int main(int, char **)
{
    LoggerManager::instance().addAppender("file",new FileAppender("riky-herd.log",LogLevel::LEVEL_TRACE,"",true));

    //testBTree();
    //testBTreeSerialize();
    //testBTreeArray(); 
    //testMemoryPage();
    //testBTStoreTests();
    testDbFile();
    
    return 0;

}
