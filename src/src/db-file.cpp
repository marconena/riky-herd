#include "db-file.h"
#include <stdio.h>
#include <stdlib.h>
#include <system.h>


const char *HEADER_STRING="ricky-herd";
byte VERSION[3]={0,0,1};

FileHeader::FileHeader(): _dirty(false) {
    push(HEADER_STRING,(MemSize)strlen(HEADER_STRING));
    push(VERSION,sizeof(VERSION));
    MemSize p=size();
    alloc(size()+_index.size()*3+sizeof(PageId)+4);

    _index.linkTo(buf()+p);
    p+=_index.size();
    _free.linkTo(buf()+p);
    p+=_free.size();
    _data.linkTo(buf()+p);
    p+=_data.size();
    _pageCount=(PageId*)((char *)buf() + p);
    *_pageCount=0;
    p+=sizeof(PageId);
    set(p,">>>>",4);
}

PageLink &FileHeader::indexLink() {
    return _index;
}
PageLink &FileHeader::freeLink() {
    return _free;
}
PageLink &FileHeader::dataLink() {
    return _data;
}

void FileHeader::indexLink(const PageLink &link) {
    if(_index!=link) {
        _index=link;
        _dirty=true;
    }
}
void FileHeader::freeLink(const PageLink &link) {
    if(_free!=link) {
        _free=link;
        _dirty=true;
    }
}
void FileHeader::dataLink(const PageLink &link) {
    if(_data!=link) {
        _data=link;
        _dirty=true;
    }
}

PageId FileHeader::pageCount() {
    return *_pageCount;
}

PageId FileHeader::newId() {
    (*_pageCount)++;
    _dirty=true;
    return *_pageCount;
}

DBFile::DBFile(const std::string path): _path(path), _cache(100,0,this), _data(this), _index(this) {

}

DBFile::~DBFile() {
    close();
}

void DBFile::close() {
    if(_file.is_open()) {
        _file.close();
    }

}

bool DBFile::retrieve(StorageBTNode *node) {
    FileStorageNode *n((FileStorageNode*)node);
    if(n->toLoad()) {
        ByteBuf buf;
        if(_cache.get(n->pageLink(),buf)) {
            MemoryIterator<byte> i(buf);
            n->deserialize(i);
        }
        n->toLoad(false);
        return true;

    }
    return false;
}

const std::string DBFile::read(FileStorageTree &tree, const PageLink &link) {
    ByteBuf buf;
    if(!_cache.get(link,buf)) {
        return "Cannot read data from file";
    }
    tree.pageLink(link);
    MemoryIterator<byte> iter(buf);
    tree.deserialize(iter);
    tree.root()->pageLink(tree.rootLink());
    tree.root()->retrieve();
    
    return "";
}

bool DBFile::openOrCreate() {
    if(System::ExistFile(_path)) {
        // Load current file
        _file.open(_path.c_str(),std::ios::in | std::ios::out | std::ios::binary);
        if(_file.is_open()) {
            _file.read((char*)_header.buf(),_header.size());
            std::string error;
            error=read(_data,_header.dataLink());
            if(error.length()>0) {
                _error=error;
                return false;
            }
            error=read(_index,_header.indexLink());
            if(error.length()>0) {
                _error=error;
                return false;
            }
            

            return true;
        }
    } else {
        // Create new file
        _file.open(_path.c_str(),std::ios::out | std::ios::binary );
        if(_file.is_open()) {
            
            commit();
            _file.close();
            _file.open(_path.c_str(),std::ios::in | std::ios::out | std::ios::binary);


            if(_file.is_open()) {
                return true;
            }
        }

    }
    _error=strerror(errno);

    return false;
}


bool DBFile::write(FileStorageTree &tree) {
    ByteBuf buf;
    if(!tree.root()->pageLink().good()) {
        tree.serialize(buf);
        _cache.push(buf,tree.pageLink());
        tree.dirty(true);
        
    }
    if(tree.dirty()) {
        if(tree.pageLink().good()) {
            if(!(tree.root()->pageLink().good())) {
                tree.root()->nodeCreated();
                tree.rootLink(tree.root()->pageLink());   
            }
            buf.clear();
            tree.serialize(buf);
            if(!_cache.push(buf,tree.pageLink())) {
                return false;
            }
            tree.dirty(false);
            return true;
        } 
    }
    return false;
}

bool DBFile::commit()
{
    ByteBuf buf;
    
    if(write(_data)) {
        _header.dataLink(_data.pageLink());
    }
    if(write(_index)) {

        _header.indexLink(_index.pageLink());
    }
    if(_header.dirty()) {
        _file.seekp(0);
        _file.write((const char *)_header.buf(),_header.size());
        _header.dirty(false);
        _file.flush();
    }
    bool ret(false);
    std::vector<PageCache*> pages=_cache.getDirtyPages();

    for(PageCache *page:pages) {
        PageId id(page->id());
        std::streampos pos(_header.size()+(id-1)*page->size());
        _file.seekp(pos);
        _file.write((const char*)page->buf(),page->size());
        page->dirty(false);
        /*
        FILE *f=fopen("page.bin","wb");
        fwrite(page->buf(),page->size(),1,f);
        fclose(f);
        */
    }
    if(pages.size()>0) {
        _file.flush();
    }

    return pages.size()>0;

}


bool DBFile::push(const ByteBuf &buf,PageLink &link) {
    return _cache.push(buf,link);
}

bool DBFile::push(const std::string &key, const std::string &value) {
    bool ret( _data.put(StringKey(key.c_str()),StringVal(value.c_str())));
    return ret;
}
bool DBFile::del(const std::string &key) {
    return _data.del(StringKey(key.c_str()));

}


bool DBFile::get(const StringKey &key, StringVal &val ) {
    return _data.get(key,val);

}


void DBFile::onChanged(StorageBTNode *node, NodeStoreOperation oper) {
    switch(oper) {
        case NodeStoreOperation::CREATE:
        case NodeStoreOperation::UPDATE: {
                ByteBuf buf;
                FileStorageNode *n((FileStorageNode*)node);
                node->serialize(buf);
                PageLink link;
                link=n->pageLink();
                if(_cache.push(buf,link)) {
                    n->pageLink(link);
                }

            }
            break;
        case NodeStoreOperation::DELETE: {
                FileStorageNode *n((FileStorageNode*)node);
                printf("Delete NODE (%s:%i)!!!\n",n->leaf() ? "LEAF":"NODE",n->size());
                _cache.del(n->pageLink());
            }
            
            break;

    }
    
}

PageCache *DBFile::getPage(PageId id) {
    if(_file.is_open()) {
        std::streampos pos(_header.size()+(id-1)*PAGE_CACHE_SIZE);
        _file.seekp(pos);
        if(pos==_file.tellp()) {
            PageCache *ret=new PageCache(0);
            _file.read((char*)ret->buf(),ret->size());
            ret->id(id);
            ret->dirty(false);

            return ret;
        }

    }
    return NULL;

}