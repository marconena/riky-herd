# C++ types

* char: 1 bytes (-128,127)
* char unsigned: 1 bites (0,255)
* short: 2 bytes (-32'768,32'767)
* short unsigned: 2 bites (0, 65'535)
* int: 4 bytes (-2'147'483'648,2'147'483'647)
* int unsigned: 4 bytes (0,4'294'967'295)
* long long: 8 bytes,(-9'223'372'036'854'775'808,9'223'372'036'854'775'807)
* long long unsigned: 8 bytes (0,18'446'744'073'709'551'615)

